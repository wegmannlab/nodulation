/*
 *  Nodulation
 *  Author: Daniel Wegmann
 */

#include "coretools/Main/TMain.h"

//---------------------------------------------------------------------------
// Includes for tasks
//---------------------------------------------------------------------------

#include "TSimulator.h"
#include "TEstimator.h"
#include "TSummarizer.h"
#include "TFstEstimator.h"

//---------------------------------------------------------------------------
// Existing Tasks
//---------------------------------------------------------------------------

void addTaks(TMain & main) {
	// Tasks consist of a name and a pointer to a TTask object.
    // Use main.addRegularTask() to add a regular task (shown in list of available tasks)
	// Use main.addDebugTask()   to add a debug task (not shown in list of available tasks)

	//regular tasks
	main.addRegularTask("simulate", new TTask_simulate());
	main.addRegularTask("estimate", new TTask_estimate());
	main.addRegularTask("statePosteriors", new Task_statePosteriors());
	main.addRegularTask("calculateLL", new Task_calculateLL());
	main.addRegularTask("summarize", new TTask_summarize());
	main.addRegularTask("Fst", new TTask_Fst());
};

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	//Create main by providing a program name, a version, link to repo and contact email
	TMain main("nodulation", "0.1", "https://bitbucket.org/wegmannlab/nodulation", "daniel.wegmann@unifr.ch");

	//add existing tasks
	addTaks(main);

	//now run program
	return main.run(argc, argv);
};
