/*
 * NodulationTypes.h
 *
 *  Created on: Dec 17, 2020
 *      Author: phaentu
 */

#ifndef CORE_NODULATIONTYPES_H_
#define CORE_NODULATIONTYPES_H_

#include <cstdint>

//define nodulation types
typedef double NodulationPrecisionType;
typedef uint16_t NodulationNumStatesType;
typedef uint32_t NodulationLengthType;
typedef uint8_t NodulationNumTreatmentType;



#endif /* CORE_NODULATIONTYPES_H_ */
