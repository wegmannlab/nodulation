/*
 * TSummarizer.cpp
 *
 *  Created on: Apr 20, 2021
 *      Author: phaentu
 */

#include "TSummarizer.h"

using namespace coretools::instances;

//-------------------------------------
// TSummarizer
//-------------------------------------
TSummarizer::TSummarizer(){
	//reading data
	_data.initializeFromFiles();

	_outName = parameters().getParameterWithDefault("out", __GLOBAL_APPLICATION_NAME__ + "_");
	logfile().list("Will write output files with prefix '", _outName, "'. (argument 'out')");
};


void TSummarizer::run(){
	//open output file
	std::string filename = _outName + "summary.txt.gz";
	logfile().list("Writing summaries to file '", filename, "'.");

	std::vector<std::string> header = {"chr", "pos"};
	for(NodulationNumTreatmentType t = 0; t < _data.numTreatments(); ++t){
		header.push_back(_data.getTreatmentName(t) + "_het");
	}

	TOutputFile out(filename, header);

	//prepare per trement frequencies
	TPerTreatmentFrequencies frequencies(_data.numTreatments());

	//go over data and calculate summaries
	logfile().listFlushTime("Looping over all loci ...");
	for(uint32_t l = 0; l < _data.numLoci(); ++l){
		_data.writePosition(l, out);

		//get per treatment frequencies
		frequencies.clear();

		for(NodulationNumTreatmentType d = 0; d < _data.numDataSets(); ++d){
			_data[l][d].addFrequencies( frequencies[ _data.treatmentIndex(d) ] );
		}

		//normalize
		frequencies.normalize();

		//calculate heterozygosity
		for(NodulationNumTreatmentType t = 0; t < _data.numTreatments(); ++t){
			out << frequencies[t].heterozygoisty();
		}


		//end line
		out.endln();
	}

	//end
	logfile().doneTime();
};



