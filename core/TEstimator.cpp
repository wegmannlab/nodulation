/*
 * TEstimator.cpp
 *
 *  Created on: Dec 8, 2020
 *      Author: phaentu
 */

#include "TEstimator.h"
#include "TAlphaParserParser.h"

using namespace coretools::instances;
using stattools::TDataVector;

//------------------------------------------------
// TLatenVariable_polya
//------------------------------------------------
TLatenVariable_polya::TLatenVariable_polya(){
	_firstEMIteration = true;

	//reading data
	_data.initializeFromFiles();
	_data.sortCountsByWithinTreatmentFrequencies();

    //DEBUG
    //_data.write("ORDERED_", _logfile);
    //DEBUG

	//initialize mixture distribution
	logfile().startIndent("Initializing mixture model:");
	if(parameters().parameterExists("knownAlpha")){
		TAlphaParser alphaParser(_data.numTreatments(), parameters().getParameter("knownAlpha"));
		createMixture(_mixture, alphaParser);
		_updateMixtures = false;
	} else if(parameters().parameterExists("alpha")){
		TAlphaParser alphaParser(_data.numTreatments(), parameters().getParameter("alpha"));
		createMixture(_mixture, alphaParser);
		_updateMixtures = true;
	} else {
		bool perTreatmentalpha = false;
		if(parameters().parameterExists("perTreatmentAlpha")){
			perTreatmentalpha = true;
			logfile().list("Will allow for different alpha values for each treatment. (parameter 'perTreatmentAlpha')");
		} else {
			logfile().list("Will use the same alpha values for all treatments. (allow individual values with 'perTreatmentAlpha')");
		}

		if(parameters().parameterExists("polya")){
			logfile().list("Will use a mixture of Polya distributions with four parameters.");
			if(perTreatmentalpha){
				_mixture = std::make_unique< TMixtureIndividualParams<TPolya> >(_data.numTreatments());
			} else {
				_mixture = std::make_unique< TMixture<TPolya> >();
			}
			_readCommandLineArgs_Minka();
		} else {
			logfile().list("Will use a mixture of beta-binomial distributions with two parameters.");
			if(perTreatmentalpha){
				_mixture = std::make_unique< TMixtureIndividualParams<TBetaBinomial> >(_data.numTreatments());
			} else {
				_mixture = std::make_unique< TMixture<TBetaBinomial> >();
			}
		}

		uint8_t numMixtures = parameters().getParameterWithDefault("numMixtures", (uint8_t) 2);
		logfile().list("Will use a mixture of ", numMixtures, " distributions.");
		_mixture->addRandomMixtures(numMixtures);
		_updateMixtures = true;
	}

	// choose optimization scheme for alpha
	if(_mixture->distName() == TBetaBinomial::name() && parameters().parameterExists("nelderMead")){
		logfile().list("Will use the Nelder-Mead algorithm for optimization of alpha in EM.");
		_readCommandLineArgs_NelderMead();
	} else {
		logfile().list("Will use the fix-point Minka algorithm for optimization of alpha in EM.");
		_readCommandLineArgs_Minka();
	}

	_mixture->reportCurrentParameters(_data);

	//initialize transition matrix
	double rho = parameters().getParameterWithDefault("rho", 0.01);
	double rhobar = parameters().getParameterWithDefault("rhobar", 0.01);
	logfile().list("Will initialize the transition matrix with rho=", rho, " and rhobar=", rhobar, ".");
	_transitionMatrix.update(_mixture->numMixtures(), _data.numTreatments(), rho, rhobar);

	//report initial parameter values
	logfile().startIndent("Initial parameter values:");
	_reportCurrentParameterEstimates();
	logfile().endIndent();
	logfile().endIndent();
};

void TLatenVariable_polya::_readCommandLineArgs_Minka(){
    _doMinka = true;

    // read parameters for Minka
    _minDiffPolyaOptimization = parameters().getParameterWithDefault("deltaAlpha", 0.00000001);
    logfile().list("Will run the fix-point algorithm for the optimization until deltaAlpha < ", _minDiffPolyaOptimization, ". (argument 'deltaAlpha')");
    _maxNumIterationsPolyaOptimization = parameters().getParameterWithDefault("iterationsMinka", 1000);
    logfile().list("Will run at max ", _maxNumIterationsPolyaOptimization, " fix-point iterations for the optimization. (argument 'iterationsMinka')");
}

void TLatenVariable_polya::_readCommandLineArgs_NelderMead(){
    _doMinka = false;

    // read parameters for Nelder Mead
    _initialDisplacement_NelderMead = parameters().getParameterWithDefault("initialDisplacement", 10.);
    logfile().list("Will use an initial displacement of ", _initialDisplacement_NelderMead, " (argument 'initialDisplacement') for first proposal in Nelder-Mead in first EM iteration.");

    logfile().startIndent("Will dynamically adjust the threshold criteria for Nelder Mead, using the following parameters:");
	_fractionalConvergenceTolerance_NelderMead = parameters().getParameterWithDefault("tolFirstIteration", 10e-10);
	_maxNumFuncEvaluations_NelderMead = parameters().getParameterWithDefault("maxIterFirstIteration", 1000);
	logfile().list("Will run the Nelder-Mead algorithm in the first EM iteration until fractional range < ", _fractionalConvergenceTolerance_NelderMead, " (argument 'tolFirstIteration') or until a maximum of ", _maxNumFuncEvaluations_NelderMead, " has been reached (argument 'maxIterFirstIteration')." );

	_factorDynamicTolerance_NelderMead = parameters().getParameterWithDefault("factorTolerance", 0.0001);
	_numFunctionCallsUntilAdjustingTolerance_NelderMead = parameters().getParameterWithDefault("numIterAdjustment", 10);
	logfile().list("Will run the Nelder-Mead algorithm in all subsequent EM iterations until estimate is a factor of ", _factorDynamicTolerance_NelderMead, " times more precise than estimate of previous iteration (argument 'factorTolerance') after evaluating ", _numFunctionCallsUntilAdjustingTolerance_NelderMead, " function calls (argument 'numIterAdjustment')." );
	logfile().endIndent();
}

void TLatenVariable_polya::calculateEmissionProbabilities(NodulationLengthType Index, TDataVector<NodulationPrecisionType, NodulationNumStatesType> & Emission) const{
	//calculate emission probabilities

	//DEBUG
	//std::cout << "Emission[" << Index << "]:";
	//DEBUG

	for(NodulationNumStatesType s = 0; s < _transitionMatrix.numStates(); ++s){
		//DEBUG
		//std::cout << " " << s << ") ";
		//DEBUG

		Emission[s] = 0.0;
		for(NodulationNumTreatmentType d = 0; d < _data.numDataSets(); ++d){
			NodulationNumTreatmentType m = _transitionMatrix.mixtureOfTreatment(s, _data.treatmentIndex(d));
			Emission[s] += _mixture->densityLog(_data[Index][d], m, _data.treatmentIndex(d));
			//Emission[s] += (*_mixture)[m].densityLog(_data[Index][d]);

			//DEBUG
			//std::cout << " (" << (int) _data[Index][d].sum() << "|" << Emission[s] << ")";
			//DEBUG
		}

		constexpr double limit = log( std::numeric_limits<NodulationPrecisionType>::min() );
		if(Emission[s] < limit){
			Emission[s] = std::numeric_limits<NodulationPrecisionType>::min();
		} else {
			Emission[s] = exp(Emission[s]);
		}

		//DEBUG
		//std::cout << " " << Emission[s];
		//DEBUG
	}

	//DEBUG
	//std::cout << std::endl;
	//DEBUG
};

void TLatenVariable_polya::prepareEMParameterEstimationOneIteration(){
	_polyaPosteriorWeights.resize(_data.numTreatments(), _data.numLoci(), _mixture->numMixtures());
	_polyaPosteriorWeights.begin();
};

void TLatenVariable_polya::handleEMParameterEstimationOneIteration(NodulationLengthType, const TDataVector<NodulationPrecisionType, NodulationNumStatesType> & Weights){
	//store mixture posterior probability per treatment per locus
	_transitionMatrix.fillSumByMixture(Weights, _polyaPosteriorWeights);
	_polyaPosteriorWeights.advance();
};

void TLatenVariable_polya::finalizeEMParameterEstimationOneIteration(){
	//DEBUG
	//_polyaPosteriorWeights.print();
	//DEBUG
	if(_updateMixtures){
		if (_doMinka){
			_mixture->updateParametersEM_Minka(_polyaPosteriorWeights, _data, _minDiffPolyaOptimization, _maxNumIterationsPolyaOptimization);
		} else {
			_mixture->updateParametersEM_NelderMead(_polyaPosteriorWeights, _data, _firstEMIteration, _fractionalConvergenceTolerance_NelderMead, _maxNumFuncEvaluations_NelderMead, _factorDynamicTolerance_NelderMead, _numFunctionCallsUntilAdjustingTolerance_NelderMead, _initialDisplacement_NelderMead);
		}
	}

	// switch bool
	if (_firstEMIteration){
	    _firstEMIteration = false;
	}
};

void TLatenVariable_polya::reportEMParameters(){
	logfile().startIndent("Current Parameter estimates:");
	_reportCurrentParameterEstimates();
	logfile().endIndent();
};

void TLatenVariable_polya::_reportCurrentParameterEstimates(){
	//Polya distributions
	_mixture->reportCurrentParameters(_data);

	//initial states
	logfile().startIndent("Initial states:");
	logfile().list("a = ", coretools::str::concatenateString(_transitionMatrix.initialProbabilities(), ","));
	logfile().endIndent();

	//transition matrix
	logfile().startIndent("Transition rates:");
	logfile().list("rho = ", _transitionMatrix.rho());
	logfile().list("rhobar = ", _transitionMatrix.rhobar());
	logfile().endIndent();
};

void TLatenVariable_polya::prepareStatePosteriorEstimationAndWriting(NodulationNumStatesType, std::vector<std::string> & header){
	//prepare tmp vars
	_polyaPosteriorWeights.resize(_data.numTreatments(), 1, _mixture->numMixtures());
	_polyaPosteriorWeights.begin();

	//assemble header
	header = { (std::string) TData::chrTag, (std::string) TData::posTag, "isUniform"};

	//per treatment posterior probabilities
	for(uint8_t t = 0; t < _data.numTreatments(); ++t){
		for(uint8_t d = 0; d < _mixture->numMixtures(); ++d){
			header.push_back(_data.getTreatmentName(t) + "_" + coretools::str::toString(d+1));
		}
	}

	//per state posterior probabilities
	for(uint16_t s = 0; s < _transitionMatrix.numStates(); ++s){
		header.push_back("State_" + _transitionMatrix.stateString(s));
	}
};

void TLatenVariable_polya::handleStatePosteriorEstimationAndWriting(NodulationLengthType Index,
			                                                        const TDataVector<NodulationPrecisionType, NodulationNumStatesType> & StatePosteriorProbabilities,
																	coretools::TOutputFile & out){
	//write out chr and position
	_data.writePosition(Index, out);

	//get is uniform posterior
	out << _transitionMatrix.sumAcrossUniform(StatePosteriorProbabilities);

	//get per treatment
	_polyaPosteriorWeights.setCurToZero();
	_transitionMatrix.fillSumByMixture(StatePosteriorProbabilities, _polyaPosteriorWeights);
	for(NodulationNumTreatmentType t = 0; t < _data.numTreatments(); ++t){
		for(NodulationNumTreatmentType d = 0; d < _mixture->numMixtures(); ++d){
			out << _polyaPosteriorWeights[d].current(t);
		}
	}

	//per state
	for(NodulationNumStatesType s = 0; s < _transitionMatrix.numStates(); ++s){
		out << StatePosteriorProbabilities[s];
	}

	out.endln();
};

//------------------------------------------------
// TEstimator
//------------------------------------------------
TEstimator::TEstimator(){
	//create latent variable object
	_latentVariable = new TLatenVariable_polya();

	//create HMM
	_hmm = new stattools::THMM<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>(_latentVariable->transitionMatrix(), *_latentVariable);
	_hmm->report();

	//outname
	_outName = parameters().getParameterWithDefault("out", coretools::__GLOBAL_APPLICATION_NAME__ + "_");
	logfile().list("Will write output files with prefix '" + _outName + "'. (argument 'out')");
};

TEstimator::~TEstimator(){
	delete _latentVariable;
	delete _hmm;
};

void TEstimator::runBaumWelch(){
	//prepare junks in which to run HMM
	// A junk is one chromosome
	std::vector<NodulationLengthType> junkEnds;
	_latentVariable->data().fillHMMJunkEnds(junkEnds);

	//estimate hierarchical parameters
	_hmm->setEstimatePrior(true);
	_hmm->runEM(junkEnds);
	logfile().startIndent("Final parameter estimates:");
	_latentVariable->_reportCurrentParameterEstimates();
	logfile().endIndent();

	//estimate state posteriors
	std::string filename = _outName + "statePosteriors.txt.gz";
	logfile().list("Writing state posteriors to '" + filename + "'.");
	_hmm->estimateStatePosteriors(junkEnds, filename);
};


void TEstimator::estimateStatePosteriors(){
	//prepare junks in which to run HMM
	// A junk is one chromosome
	std::vector<NodulationLengthType> junkEnds;
	_latentVariable->data().fillHMMJunkEnds(junkEnds);

	//estimate state posteriors
	std::string filename = _outName + "statePosteriors.txt.gz";
	logfile().list("Writing state posteriors to '" + filename + "'.");
	_hmm->estimateStatePosteriors(junkEnds, filename);
};

void TEstimator::calculateLL(){
	//prepare junks in which to run HMM
	// A junk is one chromosome
	std::vector<NodulationLengthType> junkEnds;
	_latentVariable->data().fillHMMJunkEnds(junkEnds);

	//calculate likelihood
	_hmm->calculateLL(junkEnds);
}




