/*
 * TTransitionMatrixNodulation.cpp
 *
 *  Created on: Dec 17, 2020
 *      Author: phaentu
 */


#include "TTransitionMatrixNodulation.h"

using namespace coretools::instances;

//--------------------------------------------
// TTransitionMatrix_nodulation
//--------------------------------------------
TTransitionMatrix_nodulation::TTransitionMatrix_nodulation(){
	_isInitialized = false;
	_numMixtures_D = 0;
	_numTreatments_J = 0;
	_numStates = 0;
	_numNonUniformStates = 0;

	_rho = 0.0;
	_rhobar = 0.0;
	_sigma = 0.0;
	_sigmabar = 0.0;
	_pi = 0.0;
	_pibar = 0.0;
	//_EMSum_rho = 0.0;
	//_EMSum_rhobar = 0.0;
	_EMNumAdded = 0;
};

TTransitionMatrix_nodulation::TTransitionMatrix_nodulation(NodulationNumTreatmentType NumMixtures_D, NodulationNumTreatmentType NumTreatments_J, NodulationPrecisionType Rho, NodulationPrecisionType Rhobar){
    update(NumMixtures_D, NumTreatments_J, Rho, Rhobar);
};

void TTransitionMatrix_nodulation::update(NodulationNumTreatmentType NumMixtures_D, NodulationNumTreatmentType NumTreatments_J, NodulationPrecisionType Rho, NodulationPrecisionType Rhobar){
	_numMixtures_D = NumMixtures_D;
	_numTreatments_J = NumTreatments_J;

	//check for overflow
	if(_numTreatments_J * log(_numMixtures_D) > 11){
		throw "The choices J=" + coretools::str::toString(_numTreatments_J) + " and D=" + coretools::str::toString(_numMixtures_D) + " result in too many states!";
	}
	_numStates = pow(_numMixtures_D, _numTreatments_J);
	_numNonUniformStates = _numStates - _numMixtures_D;
	_init();

	//get step between uniform states in natural numbering
	int unifInterval = 1;
	double dim = 1;
	for(NodulationNumStatesType i=1; i<_numTreatments_J; ++i){
		dim *= _numMixtures_D;
		unifInterval += dim;
	}

	//fill map to access
	_map_S_to_D.resize(_numTreatments_J);
	for(NodulationNumTreatmentType j=0; j<_numTreatments_J; ++j){
		//resize
		std::vector<NodulationNumTreatmentType>& m = _map_S_to_D[j];
		m.resize(_numStates, 0);

		//fill uniform states
		NodulationNumStatesType index = 0;
		for(; index<_numMixtures_D; ++index){
			m[index] = index;
		}

		//add non-uniform states to map
		int numUnifUpToHere = 0;
		double divisor = pow(_numMixtures_D, j);
		for(NodulationNumStatesType i = 0; i<_numStates; ++i){
			if(i % unifInterval == 0){
				//number matches a uniform state: skip
				++numUnifUpToHere;
			} else {
				//number does not match a uniform state: add
				m[index] = ((int) (i / divisor) % _numMixtures_D); // - numUnifUpToHere;
				++index;
			}
		}
	}

	_isInitialized = true;

	//update parameters
	update(Rho, Rhobar);
};

void TTransitionMatrix_nodulation::update(NodulationPrecisionType Rho, NodulationPrecisionType Rhobar){
	if(!_isInitialized){
		throw std::runtime_error("void TTransitionMatrix_nodulation::update(double Rho, double Rhobar): not yet initialized!");
	}
	_rho = Rho;
	_rhobar = Rhobar;

	//check if values are valid
	if(((_numMixtures_D-1) * _rho + _numNonUniformStates * _rhobar > 1.0) || (_numMixtures_D * _rho + (_numNonUniformStates - 1) * _rhobar > 1.0)){
		throw "Invalid choice of rho = " + coretools::str::toString(Rho) + " and rhobar = " + coretools::str::toString(_rhobar) + ": negative probability of no transition!";
	}

	//calculate additional terms
	_calculateSigmaAndStationary();
};

void TTransitionMatrix_nodulation::_calculateSigmaAndStationary(){
	//sigma
    _sigma = 1.0 - (_numMixtures_D - 1) * _rho - (_numStates - _numMixtures_D) * _rhobar;
    _sigmabar = 1.0 - _numMixtures_D * _rho - (_numStates - _numMixtures_D - 1) * _rhobar;

    //stationary distribution
    double denominator = _numMixtures_D * _rho + _numNonUniformStates * _rhobar;
    _pi = _numMixtures_D * _rho / denominator;
    _pibar = _numNonUniformStates *_rhobar / denominator;
};

NodulationNumTreatmentType TTransitionMatrix_nodulation::mixtureOfTreatment(NodulationNumStatesType State, NodulationNumTreatmentType Treatment) const{
	return _map_S_to_D[Treatment][State];
};

std::string TTransitionMatrix_nodulation::stateString(NodulationNumStatesType State){
	std::string s;
    for(NodulationNumStatesType j=0; j<_numTreatments_J; ++j){
    	if(j > 0){
    		s += "_";
        }
    	s += coretools::str::toString(_map_S_to_D[j][State]);
    }
    return s;
};

// Probabilites

NodulationPrecisionType TTransitionMatrix_nodulation::operator()(NodulationLengthType, NodulationNumStatesType From, NodulationNumStatesType To) const{
    //is there a jump?
    if(From == To){
        //jump to a uniform state?
        if(To < _numMixtures_D){
            return _sigma;
        } else {
            return _sigmabar;
        }
    } else {
        //jump to a uniform state?
        if(To < _numMixtures_D){
            return _rho;
        } else {
            return _rhobar;
        }
    }
};

NodulationPrecisionType TTransitionMatrix_nodulation::stationary(NodulationNumStatesType State) const {
	if(stateIsUniform(State)){
		return _pi;
	} else {
		return _pibar;
	}
};

//EM update
void TTransitionMatrix_nodulation::prepareEMParameterEstimationOneIterationTransitionProbabilities(){
	_EMSum_rho.resize(_numStates);
	_EMSum_rhobar.resize(_numStates);
	std::fill(_EMSum_rho.begin(), _EMSum_rho.end(), 0.0);
	std::fill(_EMSum_rhobar.begin(), _EMSum_rhobar.end(), 0.0);
	_EMNumAdded = 0;
};

void TTransitionMatrix_nodulation::handleEMParameterEstimationOneIterationTransitionProbabilities(NodulationLengthType, const stattools::THMMPosteriorXi<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType> & xi){
	for(NodulationNumStatesType i = 0; i < _numStates; ++i){
		for(NodulationNumStatesType j = 0; j < _numMixtures_D; ++j){
			if(i != j){
				_EMSum_rho[i] += xi(i, j);
			}
		}
		for(NodulationNumStatesType j = _numMixtures_D; j < _numStates; ++j){
			if(i != j){
				_EMSum_rhobar[i] += xi(i, j);
			}
		}
	}
	++_EMNumAdded;
};

void TTransitionMatrix_nodulation::finalizeEMParameterEstimationOneIterationTransitionProbabilities(){
	NodulationPrecisionType weighted_rho_sum = 0.0;
	NodulationPrecisionType weighted_rhobar_sum = 0.0;
	for(NodulationNumStatesType i = 0; i < _numMixtures_D; ++i){
		weighted_rho_sum +=  _EMSum_rho[i] / (NodulationPrecisionType) (_numMixtures_D - 1); //diag is not rho
		weighted_rhobar_sum +=  _EMSum_rhobar[i] / (NodulationPrecisionType) (_numStates - _numMixtures_D); //no diag for first D elements
	}
	for(NodulationNumStatesType i = _numMixtures_D; i < _numStates; ++i){
		weighted_rho_sum +=  _EMSum_rho[i] / (NodulationPrecisionType) _numMixtures_D; //no diag for latter elements
		weighted_rhobar_sum +=  _EMSum_rhobar[i] / (NodulationPrecisionType) (_numStates - _numMixtures_D - 1); //diag is not rhobar
	}

	_rho = weighted_rho_sum / (NodulationPrecisionType) _EMNumAdded;
	_rhobar = weighted_rhobar_sum / (NodulationPrecisionType) _EMNumAdded;

	//ensure that there is no underflow
	if(_rho < TRANSMATMINRHO){
		_rho = TRANSMATMINRHO;
	}
	if(_rhobar < TRANSMATMINRHO){
		_rhobar = TRANSMATMINRHO;
	}

	//calculate additional terms
	_calculateSigmaAndStationary();
};

void TTransitionMatrix_nodulation::fillSumByMixture(const stattools::TDataVector<NodulationPrecisionType, NodulationNumStatesType> & Gamma, TPolyaPosteriorWeights & WeightsByMixtureAndTreatment){
	WeightsByMixtureAndTreatment.setCurToZero();

	//loop over gamma and add to corresponding state
	for(NodulationNumTreatmentType t = 0; t < _numTreatments_J; ++t){
		for(NodulationNumStatesType s = 0; s < _numStates; ++s){
			WeightsByMixtureAndTreatment[ _map_S_to_D[t][s] ].current(t) += Gamma[s];
		}
	}
};

// Simulation

NodulationNumStatesType TTransitionMatrix_nodulation::sampleFromStationary() const{
	if(randomGenerator().getRand() < _pi){
		//return a uniform state
		return randomGenerator().sample(_numMixtures_D);
	} else {
		//return a non-uniform state
		return _numMixtures_D + randomGenerator().sample(_numNonUniformStates);
	}
};

NodulationNumStatesType TTransitionMatrix_nodulation::sampleNextState(NodulationLengthType Index, NodulationNumStatesType State) const{
	double r = randomGenerator().getRand();

	if(stateIsUniform(State)){
		//do we stay?
		if(r < _sigma){
			return State;
		}

		//do we transition to another uniform?
		if(r < _sigma + (_numMixtures_D - 1) * _rho){
			NodulationNumStatesType s = State;
			while(s == State){
				s = randomGenerator().sample(_numMixtures_D);
			}
			return s;
		}

		//we transition to a non-uniform state
		return _numMixtures_D + randomGenerator().sample(_numNonUniformStates);
	} else {
		//do we stay?
		if(r < _sigmabar){
			return State;
		}

		//do we transition to a uniform state?
		if(r < _sigmabar + _numMixtures_D * _rho){
			return randomGenerator().sample(_numMixtures_D);
		}

		//we transition to a non-uniform state
		NodulationNumStatesType s = State;
		while(s == State){
			s = _numMixtures_D + randomGenerator().sample(_numNonUniformStates);
		}
		return s;
	}
};

// Printing

void TTransitionMatrix_nodulation::printStoDMap(){
    for(NodulationNumStatesType s=0; s<_numStates; ++s){
        std::cout << "State " << s << ":";
        for(NodulationNumStatesType j=0; j<_numTreatments_J; ++j){
            std::cout << " " << (int) _map_S_to_D[j][s];
        }
        std::cout << std::endl;
    }
};



