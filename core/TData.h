
#ifndef TDATA_H
#define TDATA_H

#include <vector>
#include <numeric>
#include <algorithm>
#include "coretools/Main/TParameters.h"
#include "coretools/Main/TLog.h"
#include "coretools/Math/mathFunctions.h"
#include "coretools/algorithms.h"
#include "coretools/Files/TOutputFile.h"
#include <limits>
#include "NodulationTypes.h"

//-------------------------------------
// TFrequencies
// nucleotide frequencies by treatment
//-------------------------------------
class TFrequencies{
private:
	std::array<double,4> _frequencies; //of A,C,G and T

public:
	TFrequencies(){
		clear();
	};

	~TFrequencies() = default;

	void clear(){
		_frequencies[0] = 0.0;
		_frequencies[1] = 0.0;
		_frequencies[2] = 0.0;
		_frequencies[3] = 0.0;
	};

	size_t size() const{
		return 4;
	};

	double& operator[](const uint8_t & index){
		return _frequencies[index];
	};

	const double& operator[](const uint8_t & index) const{
		return _frequencies[index];
	};

	void add(const TFrequencies & Other){
		_frequencies[0] += Other._frequencies[0];
		_frequencies[1] += Other._frequencies[1];
		_frequencies[2] += Other._frequencies[2];
		_frequencies[3] += Other._frequencies[3];
	};

	void normalize(){
		double sum = _frequencies[0] + _frequencies[1] + _frequencies[2] + _frequencies[3];
		_frequencies[0] /= sum;
		_frequencies[1] /= sum;
		_frequencies[2] /= sum;
		_frequencies[3] /= sum;
	};

	double heterozygoisty(){
		return 1.0 - _frequencies[0]*_frequencies[0] - _frequencies[1]*_frequencies[1] - _frequencies[2]*_frequencies[2] - _frequencies[3]*_frequencies[3];
	};

	void print(){
		std::cout << _frequencies[0] << "," << _frequencies[1] << "," << _frequencies[2] << "," << _frequencies[3];	};
};


//--------------------------------------------
// TDataCounts
//--------------------------------------------
class TDataCounts{
private:
    std::array<uint16_t,4> _counts;
    uint16_t _sum;

public:
    TDataCounts(){
    	clear();
    };
    TDataCounts(const uint16_t & numA, const uint16_t & numC, const uint16_t & numG, const uint16_t & numT){
    	update(numA, numC, numG, numT);
    };

    ~TDataCounts() = default;

    void clear(){
    	std::fill(_counts.begin(), _counts.end(), 0.0);
    	_sum = 0;
    };
    void update(const uint16_t & numA, const uint16_t & numC, const uint16_t & numG, const uint16_t & numT);
    template <typename T> void update(const T & Counts){
    	update(Counts[0], Counts[1], Counts[2], Counts[3]);
    };
    void addFrequencies(TFrequencies & frequencies);
    void sort(const std::vector<uint8_t> & index);
    const uint16_t& operator[](uint8_t k) const { return _counts[k]; };
    const uint16_t& sum() const { return _sum; };
    size_t size() const { return _counts.size(); };
    std::array<uint16_t, 4>::iterator begin(){ return _counts.begin(); };
    std::array<uint16_t, 4>::iterator end(){ return _counts.end(); };
};

//-------------------------------------
// TDataLocus
//-------------------------------------
class TDataLocus{
private:
	std::vector<TDataCounts> _replicates;

public:
	TDataLocus(const uint8_t & Size){
		_replicates.resize(Size);
	};
	~TDataLocus() = default;

	const TDataCounts& operator[](const uint8_t & index) const { return _replicates[index]; };
	TDataCounts& operator[](const uint8_t & index){ return _replicates[index]; };
	size_t size() const { return _replicates.size(); };
};

//--------------------------------------------
// TDataTreatment
//--------------------------------------------
class TDataTreatment{
private:
    std::string _name;
    std::vector<std::string> _replicates;

public:
    TDataTreatment(const std::string & Name){
        _name = Name;        
    };
    
    ~TDataTreatment()=default;

    void addReplicate(const std::string & name){
        _replicates.emplace_back(name);
    };

    const std::string& name() const { return _name; };
    uint8_t numReplicates(){ return _replicates.size(); };

    uint8_t findReplicatePointer(const std::string Name){
        for (size_t i = 0; i < _replicates.size(); ++i) {
            if(_replicates[i] == Name){
                return i;
            }
        }
        throw "Replicate '" + Name + "' does not exist for treatment '" + _name + "'!";
    };

    bool replicateExists(const std::string & Name){
        for(auto& r : _replicates){
            if(r == Name){
                return true;
            }
        }
        return false;
    };

    std::vector<std::string>::iterator begin(){ return _replicates.begin(); };
    std::vector<std::string>::iterator end(){ return _replicates.end(); };

    void printReplicates(coretools::TLog* logfile){
        for(auto& r : _replicates){
            logfile->list(r);
        }
    };
};

//--------------------------------------------
// TChromosome
//--------------------------------------------
class TChromosome{
private:
    std::string _name;
    uint32_t _firstPosition;
    uint32_t _lastPosition;
public:
    TChromosome(const std::string & Name, const uint32_t & FirstPosition, const uint32_t & LastPosition){
        _name = Name;
        _firstPosition = FirstPosition;
        _lastPosition =  LastPosition;
    };
    ~TChromosome() = default;

    //setters
    void incrementLastPosition(const uint32_t & increment=1){
        _lastPosition += increment;
    };

    //getters
    const std::string& name() const { return _name; };
    const uint32_t& firstPosition() const { return _firstPosition; };
    const uint32_t& lastPosition() const { return _lastPosition; };
    uint32_t length() const { return _lastPosition - _firstPosition; };
};

//--------------------------------------------
// TChromosomes
//--------------------------------------------
class TChromosomes{
private:
    std::vector<TChromosome> _chromosomes;

public:
    TChromosomes(){};
    ~TChromosomes()=default;

    void clear(){
    	_chromosomes.clear();
    };

    void addChromosome(const std::string & Name, const uint32_t & FirstPosition){
        _chromosomes.emplace_back(Name, FirstPosition, FirstPosition);
    };

    void addChromosome(const std::string & Name, const uint32_t & FirstPosition, const uint32_t & LastPosition){
		_chromosomes.emplace_back(Name, FirstPosition, LastPosition);
	};

    void incrementLastPositionOfLast(const uint32_t & increment=1){
        _chromosomes.back().incrementLastPosition(increment);
    };

    const TChromosome& operator[](size_t index) const { return _chromosomes[index]; };
    std::vector<TChromosome>::iterator begin(){ return _chromosomes.begin(); };
    std::vector<TChromosome>::iterator end(){ return _chromosomes.end(); };
    std::vector<TChromosome>::const_iterator cbegin() const { return _chromosomes.cbegin(); };
    std::vector<TChromosome>::const_iterator cend() const { return _chromosomes.cend(); };

    uint32_t totalLength() const{
        uint32_t tot = 0;
        for(auto& c : _chromosomes){
            tot += c.length();
        }
        return tot;
    };

    uint32_t size() const{
        return _chromosomes.size();
    };
};

//--------------------------------------------
// TDataFile
//--------------------------------------------
class TDataFile{
private:
	uint8_t _index;
    coretools::TInputFile _file;
    std::vector<std::string> _line;
    uint32_t _position;

public:    
    TDataFile(const std::string & Filename, const uint8_t & Index);
    ~TDataFile() = default;

    std::string name() const { return _file.name(); };
    const uint32_t& position() const { return _position; };
    const std::string& chr() const { return _line[0]; };
    bool isOpen() const { return _file.isOpen(); };

    bool advance();
    void saveData(const std::string & Chr, const uint32_t & Position, TDataLocus & Locus);
};

//--------------------------------------------
// TData
//--------------------------------------------
class TData{
private:
	bool _isInitialized;
	TChromosomes _chromosomes;

	std::vector<TDataTreatment> _treatments;
	std::vector<TDataLocus> _loci;

	NodulationNumTreatmentType _numDataSets; //total across all treatments
	std::vector<NodulationNumTreatmentType> _indexToTreatment; //maps the index across data sets to the corresponding treatment index

	TDataTreatment& _findTreatment(const std::string & name);
	bool _treatmentExists(const std::string & name);
	void _fillIndexMap();

	//functions to read and write data
	void _savePosition(std::vector<TDataFile*> & DataFiles, const std::string & Chr, const uint32_t & Position);
	void _jumpToNextChromosome(std::vector<TDataFile*> & DataFiles, std::string & curChr, uint32_t & curPos);

public:
	static constexpr std::string_view chrTag = "chromosome";
	static constexpr std::string_view posTag = "position";

	TData();
    TData(const uint8_t & NumTreatmenets, const uint8_t & NumReplicates, const std::vector<uint32_t> & Chromosomes);
    ~TData() = default;

    void clear();
    void initializeFromFiles();
    void initializeFromDimensions(const uint8_t & NumTreatmenets, const uint8_t & NumReplicates, const std::vector<uint32_t> & Chromosomes);
    void sortCountsByWithinTreatmentFrequencies();

    //getters
    NodulationNumTreatmentType numTreatments() const { return _treatments.size(); };
    NodulationNumTreatmentType numDataSets() const { return _numDataSets; };
    uint32_t numChromosomes() const { return _chromosomes.size(); };
    uint32_t numLoci() const { return _loci.size(); };
    const NodulationNumTreatmentType& treatmentIndex(const NodulationNumTreatmentType & dataSet) const { return _indexToTreatment[dataSet]; };
    void fillHMMJunkEnds(std::vector<uint32_t> & JunkEnds) const;
    const std::string getTreatmentName(const NodulationNumTreatmentType & Treatment) const { return _treatments[Treatment].name(); };
    std::vector<size_t> getDataIndecesAllTreatments() const;
    std::vector<size_t> getDataIndeces(NodulationNumTreatmentType Treatment) const;

    const TDataLocus& operator[](const uint32_t & locus) const { return _loci[locus]; };
    TDataLocus& operator[](const uint32_t & locus){ return _loci[locus]; };

    const TChromosomes& chromosomes() const { return _chromosomes; };

    //output
    void write(const std::string & BaseName);
    void writePosition(const uint32_t & locus, coretools::TOutputFile & out);
};

#endif // TDATA_H
