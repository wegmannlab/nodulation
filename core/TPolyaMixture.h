

#include <array>
#include <vector>
#include <cstdint>

#ifndef TPOLYAMIXTURE_H_
#define TPOLYAMIXTURE_H_

#include "TData.h"
#include <algorithm>
#include <cmath>
#include "coretools/Files/TFile.h"
#include "coretools/Math/mathFunctions.h"
#include "stattools/MLEInference/TNelderMead.h"
#include "coretools/Main/TRandomGenerator.h"
#include "NodulationTypes.h"
#include "TAlphaParserParser.h"

#define POLYAMINALPHA 0.00000001

//-------------------------------------
// TPolyaPosteriorWeightsOneMixture
// Posterior Weights of Mixture per locus
//-------------------------------------
class TPolyaPosteriorWeightsOneMixture{
private:
	std::vector<double> _posteriorWeights;
	uint32_t _numLoci;
	uint8_t _numTreatments;
	mutable uint32_t _curLocusIndex;

public:
	TPolyaPosteriorWeightsOneMixture();
	TPolyaPosteriorWeightsOneMixture(const uint32_t & NumLoci, const uint8_t & NumTreatments){
		resize(NumLoci, NumTreatments);
	};
	~TPolyaPosteriorWeightsOneMixture() = default;

	void fill(const double & Value){ std::fill(_posteriorWeights.begin(), _posteriorWeights.end(), Value); };
	void resize(const uint32_t & NumLoci, const uint8_t & NumTreatments);
	size_t size() const { return _posteriorWeights.size(); };

	//loop over loci
	void begin() const { _curLocusIndex = 0; };
	bool end() const { return _curLocusIndex == _numLoci; };
	void advance() const { _curLocusIndex += _numTreatments; };

	double& current(const uint8_t & Treatment){ return _posteriorWeights[_curLocusIndex + Treatment]; };
	const double& current(const uint8_t & Treatment) const { return _posteriorWeights[_curLocusIndex + Treatment]; };
	double& operator()(const uint32_t & Locus, const uint8_t & Treatment){ return _posteriorWeights[_numTreatments * Locus + Treatment]; };
	void setCurToZero();
};

class TPolyaPosteriorWeights{
private:
	std::vector<TPolyaPosteriorWeightsOneMixture> _weights;

public:
	TPolyaPosteriorWeights() = default;
	TPolyaPosteriorWeights(const uint8_t & NumMixtures, const uint32_t & NumLoci, const uint8_t & NumTreatments){
		resize(NumTreatments, NumLoci, NumMixtures);
	};

	void resize(const uint8_t & NumTreatments, const uint32_t & NumLoci, const uint8_t & NumMixtures);
	void fill(const double & Value);

	//loop over loci
	void begin();
	bool end() const { return _weights[0].end(); }
	void advance();

	TPolyaPosteriorWeightsOneMixture& operator[](const uint8_t & Treatment){ return _weights[Treatment]; };
	const TPolyaPosteriorWeightsOneMixture& operator[](const uint8_t & Treatment) const { return _weights[Treatment]; };
	void setCurToZero();
};

//--------------------------------------------
// TDistribution
//--------------------------------------------
class TDistribution{
protected:
	std::vector<double> _alpha;
	double _alpha0;

	void _initializeRandom(){
		std::vector<double> Alpha(size());
		coretools::instances::randomGenerator().fillRand(Alpha);
		std::sort(Alpha.begin(), Alpha.end());
		set(Alpha);
	};

	void _setAlpha0(){
		_alpha0 = std::accumulate(_alpha.begin(), _alpha.end(), 0.0);
	};

	void _preventAlphaUnderflow(){
		for(uint8_t k=0; k<_alpha.size(); ++k){
			if(_alpha[k] < POLYAMINALPHA){
				_alpha[k] = POLYAMINALPHA;
			}
		}
	};

	virtual void _fillTmpVariables() = 0;


public:
	TDistribution(const uint8_t & Size){
		_alpha.resize(Size);
		std::fill(_alpha.begin(), _alpha.end(), 1.0);
		_setAlpha0();
	};

	virtual ~TDistribution(){};

	uint8_t size(){ return _alpha.size(); };

	template <typename T> void set(const T & Alpha){
		if(Alpha.size() != _alpha.size()){
			throw "Can not initialize distribution: number of alpha values provided != 4!";
		}

		for(uint8_t k=0; k<_alpha.size(); ++k){
			if(Alpha[k] <= 0.0){
				throw "Can not initialize distribution with alpha_k <= 0.0!";
			}
			_alpha[k] = Alpha[k];
		}

		_fillTmpVariables();
	};

	//access and density
	const double& operator[](uint8_t k) const { return _alpha[k]; };
	virtual double densityLog(const TDataCounts & Counts) const = 0;
	double relativeMean() const{
		//mean of first component per sample
		return _alpha[0] / std::accumulate(_alpha.begin(), _alpha.end(), 0.0);
	}

	bool operator<(const TDistribution & Other){
		return relativeMean() < Other.relativeMean();
	}

	virtual void updateParametersEM_Minka(const TPolyaPosteriorWeightsOneMixture & Weights,
			                              const TData & Data,
										  const double & MinDiff,
										  const uint16_t & MaxNumIterations,
										  const std::vector<size_t> & DataIndecies) = 0;
	virtual void updateParametersEM_NelderMead(const TPolyaPosteriorWeightsOneMixture &Weights,
			                                   const TData &Data,
											   bool FirstEMIteration,
											   double FractionalConvergenceTolerance,
											   size_t MaxNumFuncEvaluations,
											   double FactorDynamicTolerance,
											   size_t NumFunctionCallsUntilAdjustingTolerance,
											   double InitialDisplacement,
											   const std::vector<size_t> & DataIndecies) = 0;
	virtual void simulateCounts(const uint16_t & N, std::vector<uint16_t> & Counts) = 0;
    std::string getAlphaString() const{ return coretools::str::concatenateString(_alpha, ", "); };
};

//------------------------------------------------
// TBetaBinomial
//------------------------------------------------
class TBetaBinomial final : public TDistribution{
private:
	double _betaOfAlphaBeta;
	// variables for Nelder-Mead
	stattools::TSimplex _simplexOfPreviousEMIteration;

	void _fillTmpVariables();
	double _calcQEmission_NelderMead(const std::vector<double> & LogAlphas,
			                         const TPolyaPosteriorWeightsOneMixture &Weights,
									 const TData &Data,
									 const std::vector<size_t> & DataIndecies);

public:
	TBetaBinomial();
	TBetaBinomial(const std::vector<double> & Alpha);

    ~TBetaBinomial() = default;

    static const std::string name(){ return "Beta Binomial"; }

	double densityLog(const TDataCounts & Counts) const;

	void updateParametersEM_Minka(const TPolyaPosteriorWeightsOneMixture & Weights,
			                      const TData & Data,
								  const double & MinDiff,
								  const uint16_t & MaxNumIterations,
								  const std::vector<size_t> & DataIndecies) override;
	void updateParametersEM_NelderMead(const TPolyaPosteriorWeightsOneMixture &Weights,
			                           const TData &Data,
									   bool FirstEMIteration,
									   double FractionalConvergenceTolerance,
									   size_t MaxNumFuncEvaluations,
									   double FactorDynamicTolerance,
									   size_t NumFunctionCallsUntilAdjustingTolerance,
									   double InitialDisplacement,
									   const std::vector<size_t> & DataIndecies) override;

    //simulation
    void simulateCounts(const uint16_t & N, std::vector<uint16_t> & Counts){
    	Counts[0] = coretools::instances::randomGenerator().getBetaBinomialRandom(N, _alpha[0], _alpha[1]);
    	Counts[1] = N - Counts[0];
    	Counts[2] = 0;
    	Counts[3] = 0;
    };
};

//--------------------------------------------
// TPolya
//--------------------------------------------

class TPolya final : public TDistribution{
private:
    //tmp variables
    double _gammaLogAlpha0;
    double _sumGammaLogAlpha;

    void _fillTmpVariables();

public:
    TPolya();
    TPolya(const std::vector<double> & Alpha);
    ~TPolya() = default;
    
    static const std::string name(){ return "Polya"; }

    //access and density
    const double& operator[](uint8_t k) const { return _alpha[k]; };
    double densityLog(const TDataCounts & Counts) const;

    //EM estimation: fix-point scheme (Minka 2012)
    void updateParametersEM_Minka(const TPolyaPosteriorWeightsOneMixture & Weights,
    		                      const TData & Data,
								  const double & MinDiff,
								  const uint16_t & MaxNumIterations,
								  const std::vector<size_t> & DataIndecies) override;
    void updateParametersEM_NelderMead(const TPolyaPosteriorWeightsOneMixture &Weights,
    		                           const TData &Data,
									   bool FirstEMIteration,
									   double FractionalConvergenceTolerance,
									   size_t MaxNumFuncEvaluations,
									   double FactorDynamicTolerance,
									   size_t NumFunctionCallsUntilAdjustingTolerance,
									   double InitialDisplacement,
									   const std::vector<size_t> & DataIndecies) override;

    //simulation
    void simulateCounts(const uint16_t & N, std::vector<uint16_t> & Counts){
    	coretools::instances::randomGenerator().fillPolyaRandom(N, _alpha, Counts);
    };
};

//--------------------------------------------
// TMixture
//--------------------------------------------
class TMixture_base{
protected:
	//virtual TDistribution& operator[](uint8_t j) = 0;
	//virtual const TDistribution& operator[](uint8_t j) const = 0;
	virtual void _simulateCounts(const uint16_t & N, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType Treatment, std::vector<uint16_t> & res) = 0;

public:
	TMixture_base() = default;
	virtual ~TMixture_base() = default;

	virtual const std::string distName() = 0;
	virtual void addMixture() = 0;
	virtual void addMixture(const std::vector<double> & Alpha) = 0;
	void addMixtures(const std::vector<std::string> & AlphaString);
	void addRandomMixtures(uint8_t NumMixtures);

	virtual uint8_t numMixtures() const = 0;

	virtual NodulationPrecisionType densityLog(const TDataCounts & Counts, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType Treatment) const = 0;
	virtual void reportCurrentParameters(const TData & Data) = 0;

	virtual void updateParametersEM_Minka(const TPolyaPosteriorWeights & Weights,
			                              const TData & Data,
										  const double & MinDiff,
										  const uint16_t & MaxNumIterations) = 0;
	virtual void updateParametersEM_NelderMead(const TPolyaPosteriorWeights &Weights,
											   const TData &Data,
											   bool FirstEMIteration,
											   double FractionalConvergenceTolerance,
											   size_t MaxNumFuncEvaluations,
											   double FactorDynamicTolerance,
											   size_t NumFunctionCallsUntilAdjustingTolerance,
											   double InitialDisplacement) = 0;

	//simulation
    void simulateCounts(const uint16_t & N, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType Treatment, TDataCounts & Counts);

    //write
    //void write(const std::string & Filename);
};

template <typename DistType>
class TMixture final : public TMixture_base{
protected:
	std::vector<DistType> _distributions;

	void _simulateCounts(const uint16_t & N, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType, std::vector<uint16_t> & res){
		_distributions[Mixture].simulateCounts(N, res);
	}

	/*
	TDistribution& operator[](uint8_t j) { return _distributions[j]; };
	const TDistribution& operator[](uint8_t j) const { return _distributions[j]; };
	*/

public:
	TMixture() = default;
	~TMixture() = default;

	TMixture(const TAlphaParser & Parser){
		if(Parser.hasPerTreatmentArgs()){
			DEVERROR("Failed to initialize mixture: TAlphaParser has per treatment args!");
		}
		for(size_t i = 0; i < Parser.numMixtures(); ++i){
			addMixture(Parser.get(0, i));
		}
	}

	const std::string distName(){ return DistType::name(); }

	void addMixture(){
		_distributions.emplace_back();
	}
	void addMixture(const std::vector<double> & Alpha){
		_distributions.emplace_back(Alpha);
	}

	uint8_t numMixtures() const { return _distributions.size(); };

	NodulationPrecisionType densityLog(const TDataCounts & Counts, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType) const{
		return _distributions[Mixture].densityLog(Counts);
	}

	void reportCurrentParameters(const TData &){
		using coretools::instances::logfile;
		logfile().startIndent("Parameters of ", DistType::name(), " distributions:");
		for(uint8_t d = 0; d < _distributions.size(); ++d){
			logfile().list("alpha_", d+1, ": " + _distributions[d].getAlphaString());
		}
		logfile().endIndent();
	}

	void updateParametersEM_Minka(const TPolyaPosteriorWeights & Weights,
			                      const TData & Data,
								  const double & MinDiff,
								  const uint16_t & MaxNumIterations){
		//get vector of data indecies to use: use all!
		std::vector<size_t> indeces = Data.getDataIndecesAllTreatments();
		for(uint8_t d = 0; d < numMixtures(); ++d){
			_distributions[d].updateParametersEM_Minka(Weights[d], Data, MinDiff, MaxNumIterations, indeces);
		}
	};

	void updateParametersEM_NelderMead(const TPolyaPosteriorWeights &Weights, const TData &Data,
	  								   bool FirstEMIteration,
									   double FractionalConvergenceTolerance,
									   size_t MaxNumFuncEvaluations,
									   double FactorDynamicTolerance,
									   size_t NumFunctionCallsUntilAdjustingTolerance,
									   double InitialDisplacement) {
		//get vector of data indecies to use: use all!
		std::vector<size_t> indeces = Data.getDataIndecesAllTreatments();
	    for(uint8_t d = 0; d < numMixtures(); ++d){
	    	_distributions[d].updateParametersEM_NelderMead(Weights[d], Data, FirstEMIteration, FractionalConvergenceTolerance, MaxNumFuncEvaluations, FactorDynamicTolerance, NumFunctionCallsUntilAdjustingTolerance, InitialDisplacement, indeces);
	    }
	};
};

template <typename DistType>
class TMixtureIndividualParams final : public TMixture_base{
protected:
	//Will store one distribution per treatment for each mixture component
	//Thus: #distributions =  #treatments * #mixture components
	//distributions are order by treatment, then mixture component.
	//Distributions will be sorted increasingly by their mean
	std::vector< std::vector<DistType> > _distributions;
	NodulationNumTreatmentType _numMixtures{0};

	void _simulateCounts(const uint16_t & N, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType Treatment, std::vector<uint16_t> & res){
		_distributions[Treatment][Mixture].simulateCounts(N, res);
	}

	/*
	TDistribution& operator[](uint8_t j) { return _distributions[j]; };
	const TDistribution& operator[](uint8_t j) const { return _distributions[j]; };
*/

	void _sortDistributions(){
		//ensures that distributions are sorted increasingly by their mean (for each treatment individually)
		for(auto & t : _distributions){
			sort(t.begin(), t.end()); //sorts by relative mean, see above
		}
	}

public:
	TMixtureIndividualParams(NodulationNumTreatmentType NumTreatments){
		_distributions.resize(NumTreatments);
	};

	TMixtureIndividualParams(const TAlphaParser & Parser){
		_distributions.resize(Parser.numTreatments());
		if(Parser.hasPerTreatmentArgs()){
			//initialize individually
			for(size_t t = 0; t < Parser.numTreatments(); ++t){
				for(size_t i = 0; i < Parser.numMixtures(); ++i){
					_distributions[t].emplace_back(Parser.get(t, i));
				}
			}
			_numMixtures = Parser.numMixtures();
			_sortDistributions();
		} else {
			//use same to initialize each treatment
			for(size_t i = 0; i < Parser.numMixtures(); ++i){
				addMixture(Parser.get(0, i));
			}
		}
	}

	~TMixtureIndividualParams() = default;

	const std::string distName(){ return DistType::name(); }

	void addMixture(){
		//add one per treatment
		++_numMixtures;
		for(auto & t : _distributions){
			t.emplace_back();
		}
		_sortDistributions();
	}

	void addMixture(const std::vector<double> & Alpha){
		//add one per treatment
		++_numMixtures;
		for(auto & t : _distributions){
			t.emplace_back(Alpha);
		}
		_sortDistributions();
	}

	uint8_t numMixtures() const { return _numMixtures; };

	NodulationPrecisionType densityLog(const TDataCounts & Counts, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType Treatment) const{
		return _distributions[Treatment][Mixture].densityLog(Counts);
	}

	void reportCurrentParameters(const TData & Data){
		using coretools::instances::logfile;
		logfile().startIndent("Parameters of ", DistType::name(), " distributions:");
		for(NodulationNumTreatmentType i = 0; i < _distributions.size(); ++i){
			logfile().startIndent("Treatment '", Data.getTreatmentName(i), "':");
			for(size_t d = 0; d < _distributions[i].size(); ++d){
				logfile().list("alpha_", d+1, ": " + _distributions[i][d].getAlphaString());
			}
			logfile().endIndent();
		}
		logfile().endIndent();
	}

	void updateParametersEM_Minka(const TPolyaPosteriorWeights & Weights,
			                      const TData & Data,
								  const double & MinDiff,
								  const uint16_t & MaxNumIterations){

		for(NodulationNumTreatmentType i = 0; i < _distributions.size(); ++i){
			//get vector of data indecies for this treatment
			std::vector<size_t> indeces = Data.getDataIndeces(i);
			for(uint8_t d = 0; d < numMixtures(); ++d){
				_distributions[i][d].updateParametersEM_Minka(Weights[d], Data, MinDiff, MaxNumIterations, indeces);
			}
		}
	};

	void updateParametersEM_NelderMead(const TPolyaPosteriorWeights &Weights, const TData &Data,
	  								   bool FirstEMIteration,
									   double FractionalConvergenceTolerance,
									   size_t MaxNumFuncEvaluations,
									   double FactorDynamicTolerance,
									   size_t NumFunctionCallsUntilAdjustingTolerance,
									   double InitialDisplacement) {
		for(NodulationNumTreatmentType i = 0; i < _distributions.size(); ++i){
			//get vector of data indecies for this treatment
			std::vector<size_t> indeces = Data.getDataIndeces(i);
			for(uint8_t d = 0; d < numMixtures(); ++d){
				_distributions[i][d].updateParametersEM_NelderMead(Weights[d], Data, FirstEMIteration, FractionalConvergenceTolerance, MaxNumFuncEvaluations, FactorDynamicTolerance, NumFunctionCallsUntilAdjustingTolerance, InitialDisplacement, indeces);
			}
		}
	};
};

void createMixture(std::unique_ptr<TMixture_base> & Ptr, const TAlphaParser & Parser);

#endif // TPOLYAMIXTURE_H_




