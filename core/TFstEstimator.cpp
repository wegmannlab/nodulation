/*
 * TFstEstimator.cpp
 *
 *  Created on: Nov 12, 2021
 *      Author: phaentu
 */


#include "TFstEstimator.h"

using namespace coretools::instances;

TFstEstimator::TFstEstimator(){
	//reading data
	_data.initializeFromFiles();

	//depth filter
	_depthFilter = parameters().getParameterWithDefault<uint32_t>("depthFilter", 0);
	if(_depthFilter > 0){
		logfile().list("Will ignore replicates with a depth < ", _depthFilter, ". (argument 'depthFilter')");
	} else {
		logfile().list("Will use all replicates with data, i.e. with depth > 0. (use 'depthFilter' to filter on depth)");
	}

	//outname
	_outName = parameters().getParameterWithDefault("out", coretools::__GLOBAL_APPLICATION_NAME__ + "_");
	logfile().list("Will write output files with prefix '" + _outName + "'. (argument 'out')");
};

double TFstEstimator::_calcFst(const double H_T, const double H_S){
	if(H_T == 0.0){
		return 0.0;
	} else {
		return (H_T - H_S) / H_T;
	}
};

void TFstEstimator::estimateFst(){
	//open output file
	std::string filename = _outName + "Fst.txt.gz";
	logfile().list("Will write Fst estimates to '", filename, "'.");
	std::vector<std::string> myHeader = {"chr", "pos"};
	for(uint8_t i = 0; i < _data.numTreatments(); ++i){
		myHeader.push_back("Fst_" + _data.getTreatmentName(i));
	}
	myHeader.push_back("globalFst");
	coretools::TOutputFile out(filename, myHeader);

	//prepare storage
	std::vector<double> perTreatmentHet(_data.numTreatments());
	std::vector<uint8_t> numReplicatesPerTreatmentWithData(_data.numTreatments());
	std::vector<TFrequencies> perTreamentFreq(_data.numTreatments());
	TFrequencies tmpFreq;
	TFrequencies totFreq;

	//parse through data
	for(uint32_t l = 0; l < _data.numLoci(); ++l){
		//write position
		_data.writePosition(l, out);

		//reset storage
		uint8_t numTreatmentsWithData = 0;
		std::fill(perTreatmentHet.begin(), perTreatmentHet.end(), 0.0);
		std::fill(numReplicatesPerTreatmentWithData.begin(), numReplicatesPerTreatmentWithData.end(), 0);
		for(auto& f : perTreamentFreq){
			f.clear();
		}

		//loop over data sets
		for(uint8_t d = 0; d < _data.numDataSets(); ++d){
			if(_data[l][d].sum() > _depthFilter){
				uint8_t t = _data.treatmentIndex(d);

				++numReplicatesPerTreatmentWithData[t];

				//calculate frequencies
				tmpFreq.clear();
				_data[l][d].addFrequencies(tmpFreq);

				perTreatmentHet[t] += tmpFreq.heterozygoisty();
				perTreamentFreq[t].add(tmpFreq);
			}
		}

		//calculate within treatment Fst
		totFreq.clear();
		double globalHet = 0.0;
		for(uint8_t i = 0; i < _data.numTreatments(); ++i){
			//calculate per treatment Fst
			if(numReplicatesPerTreatmentWithData[i] > 1){
				perTreamentFreq[i].normalize();
				perTreatmentHet[i] /= numReplicatesPerTreatmentWithData[i];
				out << _calcFst(perTreamentFreq[i].heterozygoisty(), perTreatmentHet[i]);

				//sum up to calculate global Fst
				totFreq.add(perTreamentFreq[i]);
				globalHet += perTreatmentHet[i];
				++numTreatmentsWithData;
			} else {
				out << "NA";
			}
		}

		//calculate global Fst
		if(numTreatmentsWithData > 1){
			totFreq.normalize();
			globalHet /= _data.numTreatments();
			out.write(_calcFst(totFreq.heterozygoisty(), globalHet));
		} else {
			out.write("NA");
		}
	}
};

