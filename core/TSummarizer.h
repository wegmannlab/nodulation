/*
 * TSummarizer.h
 *
 *  Created on: Apr 20, 2021
 *      Author: phaentu
 */

#ifndef CORE_TSUMMARIZER_H_
#define CORE_TSUMMARIZER_H_

#include "coretools/Main/TLog.h"
#include "TData.h"
#include "coretools/Main/TTask.h"
#include "coretools/Main/globalConstants.h"
#include "coretools/Files/TFile.h"
#include <array>
#include "NodulationTypes.h"

using namespace coretools;

//-------------------------------------
// TPerTreatmentFrequencies
//-------------------------------------
class TPerTreatmentFrequencies{
private:
	std::vector<TFrequencies> _freq;

public:
	TPerTreatmentFrequencies(const NodulationNumTreatmentType & numTreatments){
		_freq.resize(numTreatments);
		clear();
	};
	~TPerTreatmentFrequencies() = default;

	void clear(){
		for(auto& f : _freq){
			f.clear();
		}
	};

	void normalize(){
		for(auto& f : _freq){
			f.normalize();
		}
	};

	TFrequencies& operator[](const NodulationNumTreatmentType & index){
		return _freq[index];
	};
};

//-------------------------------------
// TSummarizer
//-------------------------------------
class TSummarizer{
private:
	TData _data;
	std::string _outName;

public:
	TSummarizer();
	~TSummarizer() = default;

	void run();
};

//------------------------------------------------
// Task_summarize
//------------------------------------------------
class TTask_summarize:public TTask{
public:
	//constructor that sets explanation
	TTask_summarize(){ _explanation = "Summarize the data in terms of heterozygosity"; };

	//run function
	void run(){
		TSummarizer summarizer;
		summarizer.run();
	};
};



#endif /* CORE_TSUMMARIZER_H_ */
