
#include "TPolyaMixture.h"
#include "coretools/Main/TLog.h"

using coretools::gammaLog;
using coretools::chooseLog;
using coretools::TFactorial::factorialLog;
using coretools::TDigamma::digamma;

//-------------------------------------
// TPolyaPosteriorWeightsOneTreatment
// Posterior Weights of Mixture per locus
//-------------------------------------
TPolyaPosteriorWeightsOneMixture::TPolyaPosteriorWeightsOneMixture(){
	_numLoci = 0;
	_numTreatments = 0;
	_curLocusIndex = 0;
};

void TPolyaPosteriorWeightsOneMixture::resize(const uint32_t & NumLoci, const uint8_t & NumTreatments){
	_numLoci = NumLoci;
	_numTreatments = NumTreatments;
	_posteriorWeights.resize(_numLoci * _numTreatments);
	fill(0.0);
};

void TPolyaPosteriorWeightsOneMixture::setCurToZero(){
	for(uint8_t m = 0; m < _numTreatments; ++m){
		_posteriorWeights[_curLocusIndex + m] = 0.0;
	}
};

void TPolyaPosteriorWeights::resize(const uint8_t & NumMixtures, const uint32_t & NumLoci, const uint8_t & NumTreatments){
	_weights.resize(NumMixtures);
	for(auto& w : _weights){
		w.resize(NumLoci, NumTreatments);
	}
};

void TPolyaPosteriorWeights::fill(const double & Value){
	for(auto& w : _weights){
		w.fill(Value);
	}
};

//loop over loci
void TPolyaPosteriorWeights::begin(){
	for(auto& w : _weights){
		w.begin();
	}
};

void TPolyaPosteriorWeights::advance(){
	for(auto& w : _weights){
		w.advance();
	}
};

void TPolyaPosteriorWeights::setCurToZero(){
	for(auto& w : _weights){
		w.setCurToZero();
	}
};

//--------------------------------------------
// TBetaBinomial
//--------------------------------------------
TBetaBinomial::TBetaBinomial():TDistribution(2){
	_initializeRandom();
    _fillTmpVariables();
};

TBetaBinomial::TBetaBinomial(const std::vector<double> & Alpha):TDistribution(2){
	set(Alpha);
	_fillTmpVariables();
};

void TBetaBinomial::_fillTmpVariables(){
	_betaOfAlphaBeta = gammaLog(_alpha[0] + _alpha[1]) - gammaLog(_alpha[0]) - gammaLog(_alpha[1]);
};

double TBetaBinomial::densityLog(const TDataCounts & Counts) const{
    double densLog = chooseLog(Counts.sum(), Counts[0]);
    densLog += gammaLog(Counts[0] + _alpha[0]) + gammaLog(Counts.sum() - Counts[0] + _alpha[1]) - gammaLog(Counts.sum() + _alpha[0] + _alpha[1]);
    densLog += _betaOfAlphaBeta;

    return densLog;
}

void TBetaBinomial::updateParametersEM_NelderMead(const TPolyaPosteriorWeightsOneMixture &Weights,
		 	 	 	 	 	 	 	 	 	 	  const TData &Data,
												  bool FirstEMIteration,
												  double FractionalConvergenceTolerance,
												  size_t MaxNumFuncEvaluations,
												  double FactorDynamicTolerance,
												  size_t NumFunctionCallsUntilAdjustingTolerance,
												  double InitialDisplacement,
												  const std::vector<size_t> & DataIndecies) {
    stattools::TNelderMead nelderMead;
    if (FirstEMIteration){
        // set "absolute" termination criteria
        nelderMead.setFractionalConvergenceTolerance(FractionalConvergenceTolerance);
        nelderMead.setMaxNumFunctionEvaluations(MaxNumFuncEvaluations);
    } else {
        // set termination criteria relative result of previous EM iteration
        nelderMead.dynamicallyAdjustTolerance(_simplexOfPreviousEMIteration[0].value(), FactorDynamicTolerance, NumFunctionCallsUntilAdjustingTolerance);
    }

    auto ptr = &TBetaBinomial::_calcQEmission_NelderMead;
    stattools::TReturnCode<stattools::TVertex> result;

    if (FirstEMIteration){
        // define starting points of Nelder-Mead: log-values of alpha
        stattools::TVertex logVertex(2);
        logVertex[0] = log(_alpha[0]); logVertex[1] = log(_alpha[1]);

        // define displacement
        double logDisplacement = log(InitialDisplacement);

        result = nelderMead.minimize(logVertex, logDisplacement, *this, ptr, Weights, Data, DataIndecies);
    } else {
        // run Nelder-Mead with simplex from previous iteration
        result = nelderMead.minimize(_simplexOfPreviousEMIteration, *this, ptr, Weights, Data, DataIndecies);
    }

    // store simplex to start next EM iteration with this
    _simplexOfPreviousEMIteration = nelderMead.getCurrentSimplex();

    // report to logfile
    //coretools::instances::logfile().list(result.message());

    // set final alphas: back-transform log
    _alpha[0] = exp(result.result()[0]);
    _alpha[1] = exp(result.result()[1]);
    _fillTmpVariables();
};

double TBetaBinomial::_calcQEmission_NelderMead(const std::vector<double> & LogAlphas, const TPolyaPosteriorWeightsOneMixture &Weights, const TData &Data, const std::vector<size_t> & DataIndecies){
    // calculate relevant term for emission probabilities from the Q-function
    assert(LogAlphas.size() == 2);

    // take exp of all entries of LogAlphas to get alphas
    _alpha[0] = exp(LogAlphas[0]);
    _alpha[1] = exp(LogAlphas[1]);
    _fillTmpVariables();

    // calculate sum
    double sum = 0.0;
    //loop over loci
    Weights.begin();
    for(uint32_t l = 0; l < Data.numLoci(); ++l, Weights.advance()){
        const TDataLocus& locus = Data[l];

        //loop over data sets
        for(auto & j : DataIndecies){
            const TDataCounts& replicatesForOneTreatment = locus[j];

            //loop over s
            double weight = Weights.current(Data.treatmentIndex(j));

            sum += weight * densityLog(replicatesForOneTreatment);
        }
    }

    return -sum; // return -sum, because we want to maximize Q, but Nelder-Mead minimizes
}

void TBetaBinomial::updateParametersEM_Minka(const TPolyaPosteriorWeightsOneMixture &Weights, const TData &Data, const double &MinDiff, const uint16_t &MaxNumIterations, const std::vector<size_t> & DataIndecies) {
    //we use the fixed-point iterative scheme of Minka (20212)
    for(uint16_t i = 0; i<MaxNumIterations; ++i){
        //set to zero
        std::array<double, 2> nominator = {0.0, 0.0};
        double denominator = 0.0;

		std::array<double, 4> digammaAlpha;
		digammaAlpha[0] = digamma(_alpha[0]);
		digammaAlpha[1] = digamma(_alpha[1]);

		double digammaAlpha0 = digamma(_alpha0);

		//loop over loci
		Weights.begin();
		for(uint32_t l = 0; l < Data.numLoci(); ++l, Weights.advance()){
			const TDataLocus& locus = Data[l];

			//loop over data sets
			for(auto & d : DataIndecies){
				//loop over k
				double weight = Weights.current(Data.treatmentIndex(d));

				nominator[0] += weight * (digamma(locus[d][0] + _alpha[0]) - digammaAlpha[0]);
				uint16_t others = locus[d].sum() - locus[d][0];
				nominator[1] += weight * (digamma(others + _alpha[1]) - digammaAlpha[1]);

				denominator += weight * (digamma(locus[d].sum() + _alpha0) - digammaAlpha0);
			}
		}

		//update parameters
		double tmp = _alpha[0] * nominator[0] / denominator;
		double diff = fabs(tmp - _alpha[0]);
		_alpha[0] = tmp;

		tmp = _alpha[1] * nominator[1] / denominator;
		diff = std::max(fabs(tmp - _alpha[1]), diff);
		_alpha[1] = tmp;

		_alpha0 = _alpha[0] + _alpha[1];

		//check if we stop
		if(diff < MinDiff){
			break;
		}
	}
	//fill other variables
	_preventAlphaUnderflow();
	_fillTmpVariables();
};

//--------------------------------------------
// TPolya
//--------------------------------------------
TPolya::TPolya(const std::vector<double> & Alpha):TDistribution(4){
	set(Alpha);
	_fillTmpVariables();
};

TPolya::TPolya():TDistribution(4){
	_initializeRandom();
	_fillTmpVariables();
};

void TPolya::_fillTmpVariables(){
	//alpha_0
	_alpha0 = _alpha[0] + _alpha[1] + _alpha[2] + _alpha[3];
	_gammaLogAlpha0 = gammaLog(_alpha0);

	//Gamma(alpha)
	_sumGammaLogAlpha = gammaLog(_alpha[0]) + gammaLog(_alpha[1]) + gammaLog(_alpha[2]) + gammaLog(_alpha[3]);
};

double TPolya::densityLog(const TDataCounts & Counts) const{
    double densLog = factorialLog(Counts.sum()) + _gammaLogAlpha0 - gammaLog(Counts.sum() + _alpha0);

    densLog += gammaLog(Counts[0] + _alpha[0]) - factorialLog(Counts[0]);
    densLog += gammaLog(Counts[1] + _alpha[1]) - factorialLog(Counts[1]);
    densLog += gammaLog(Counts[2] + _alpha[2]) - factorialLog(Counts[2]);
    densLog += gammaLog(Counts[3] + _alpha[3]) - factorialLog(Counts[3]);

    return densLog - _sumGammaLogAlpha;
};

void TPolya::updateParametersEM_NelderMead(const TPolyaPosteriorWeightsOneMixture &, const TData &, bool, double, size_t, double, size_t, double, const std::vector<size_t> &) {
    throw std::runtime_error("Function updateParametersEM_NelderMead is currently not implemented for PolyA distribution!");
}

void TPolya::updateParametersEM_Minka(const TPolyaPosteriorWeightsOneMixture &Weights, const TData &Data, const double &MinDiff, const uint16_t &MaxNumIterations, const std::vector<size_t> & DataIndecies) {
	//we use the fixed-point iterative scheme of Minka (20212)
	for(uint16_t i = 0; i<MaxNumIterations; ++i){
		//set to zero
		std::array<double, 4> nominator;
		std::fill(nominator.begin(), nominator.end(), 0.0);
		double denominator = 0.0;

		std::array<double, 4> digammaAlpha;
		digammaAlpha[0] = digamma(_alpha[0]);
		digammaAlpha[1] = digamma(_alpha[1]);
		digammaAlpha[2] = digamma(_alpha[2]);
		digammaAlpha[3] = digamma(_alpha[3]);

		double digammaAlpha0 = digamma(_alpha0);

		//loop over loci
		Weights.begin();
		for(uint32_t l = 0; l < Data.numLoci(); ++l, Weights.advance()){
			const TDataLocus& locus = Data[l];

			//loop over data sets
			for(auto & d : DataIndecies){
				//loop over k
				double weight = Weights.current(Data.treatmentIndex(d));

				nominator[0] += weight * (digamma(locus[d][0] + _alpha[0]) - digammaAlpha[0]);
				nominator[1] += weight * (digamma(locus[d][1] + _alpha[1]) - digammaAlpha[1]);
				nominator[2] += weight * (digamma(locus[d][2] + _alpha[2]) - digammaAlpha[2]);
				nominator[3] += weight * (digamma(locus[d][3] + _alpha[3]) - digammaAlpha[3]);

				denominator += weight * (digamma(locus[d].sum() + _alpha0) - digammaAlpha0);
			}
		}

		//update parameters
		double tmp = _alpha[0] * nominator[0] / denominator;
		double diff = fabs(tmp - _alpha[0]);
		_alpha[0] = tmp;

		tmp = _alpha[1] * nominator[1] / denominator;
		diff = std::max(fabs(tmp - _alpha[1]), diff);
		_alpha[1] = tmp;

		tmp = _alpha[2] * nominator[2] / denominator;
		diff = std::max(fabs(tmp - _alpha[2]), diff);
		_alpha[2] = tmp;

		tmp = _alpha[3] * nominator[3] / denominator;
		diff = std::max(fabs(tmp - _alpha[3]), diff);
		_alpha[3] = tmp;

		_alpha0 = _alpha[0] + _alpha[1] + _alpha[2] + _alpha[3];

		//check if we stop
		if(diff < MinDiff){
			break;
		}
	}

	//fill other variables
	_preventAlphaUnderflow();
	_fillTmpVariables();
};

//------------------------------------------------
// TMixture
//------------------------------------------------
//add mixtures
void TMixture_base::addMixtures(const std::vector<std::string> & AlphaString){
	std::vector<double> alpha;
	for(auto& s : AlphaString){
		coretools::str::fillContainerFromString(s, alpha, ',', true, true);
		addMixture(alpha);
	}
}

void TMixture_base::addRandomMixtures(uint8_t NumMixtures){
	for(uint8_t i = 0; i < NumMixtures; ++i){
		addMixture();
	}
}

//EM estimation: fix-point scheme (Minka 2012)
void TMixture_base::simulateCounts(const uint16_t & N, NodulationNumTreatmentType Mixture, NodulationNumTreatmentType Treatment, TDataCounts & Counts){
	static std::vector<uint16_t> simulatedCounts(4, 0.0);
	_simulateCounts(N, Mixture, Treatment, simulatedCounts);
	Counts.update(simulatedCounts);
};

/*
void TMixture_base::write(const std::string & Filename){
	std::vector<std::string> header = {"Mixture"};
	for(uint8_t i=0; i < (*this)[0].size(); ++i){
		header.push_back("alpha_" + coretools::str::toString(i+1));
	}
	coretools::TOutputFile out(Filename, header);

	for(uint8_t d = 0; d < numMixtures(); ++d){
		out << d;
		for(uint8_t i=0; i < (*this)[d].size(); ++i){
			out << (*this)[d][i];
		}
		out.endln();
	}
}
*/

void createMixture(std::unique_ptr<TMixture_base> & Ptr, const TAlphaParser & Parser){
	if(Parser.hasPerTreatmentArgs()){
		if(Parser.isBetaBinom()){
			Ptr = std::make_unique< TMixtureIndividualParams<TBetaBinomial> >(Parser);
		} else {
			Ptr = std::make_unique< TMixtureIndividualParams<TPolya> >(Parser);
		}
	} else {
		if(Parser.isBetaBinom()){
			Ptr = std::make_unique< TMixture<TBetaBinomial> >(Parser);
		} else {
			Ptr = std::make_unique< TMixture<TPolya> >(Parser);
		}
	}
}

