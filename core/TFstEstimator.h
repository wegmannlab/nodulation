/*
 * TFstEstimator.h
 *
 *  Created on: Nov 12, 2021
 *      Author: phaentu
 */

#ifndef CORE_TFSTESTIMATOR_H_
#define CORE_TFSTESTIMATOR_H_

#include "TData.h"
#include "coretools/Main/globalConstants.h"
#include "coretools/Files/TFile.h"
#include "coretools/Main/TTask.h"

//-------------------------------------
// TFstEstimator
//-------------------------------------
class TFstEstimator{
private:
	TData _data;
	std::string _outName;
	uint32_t _depthFilter;

	double _calcFst(const double H_T, const double H_S);

public:
	TFstEstimator();
	~TFstEstimator() = default;

	void estimateFst();
};

//------------------------------------------------
// Task_estimate
//------------------------------------------------
class TTask_Fst:public coretools::TTask{
public:
	//constructor that sets explanation
	TTask_Fst(){ _explanation = "Estimate within and between treatment Fst"; };

	//run function
	void run(){
		TFstEstimator estimator;
		estimator.estimateFst();
	};
};


#endif /* CORE_TFSTESTIMATOR_H_ */
