/*
 * TAlphaParser.h
 *
 *  Created on: Dec 19, 2022
 *      Author: phaentu
 */

#ifndef CORE_TALPHAPARSERPARSER_H_
#define CORE_TALPHAPARSERPARSER_H_

#include "coretools/Strings/stringFunctions.h"
#include <vector>

class TAlphaOneDist{
private:
	std::vector<double> dist;
public:
	TAlphaOneDist(std::string & args){
		//split by ','
		coretools::str::fillContainerFromString<true,true>(args, dist, ',');
	}

	size_t numAlpha() const { return dist.size(); }
	const std::vector<double>& get() const { return dist; }
};

class TAlphaOneTreatment{
private:
	std::vector<TAlphaOneDist> dist;
public:
	TAlphaOneTreatment(std::string & args){
		//split by ';'
		std::vector<std::string> tmp;
		coretools::str::fillContainerFromString<true,true>(args, tmp, ';');
		for(auto& t : tmp){
			dist.emplace_back(t);
		}
	}

	size_t numMixtures(){
		return dist.size();
	}

	size_t numAlpha(){
		size_t numAlpha = dist[0].numAlpha();
		for(size_t i = 1; i < dist.size(); ++i){
			if(dist[i].numAlpha() != numAlpha){
				UERROR("Failed to initialize mixture distributions: unequal number of alpha values among mixtures!");
			}

		}
		return numAlpha;
	}

	const std::vector<double>& operator[](size_t mixtureIndex) const {
		return dist[mixtureIndex].get();
	}
};

class TAlphaParser{
private:
	std::vector<TAlphaOneTreatment> _dist;
	size_t _numTreatments;
	size_t _numMixtures;
	size_t _numAlpha;
	bool _perTreatmentArgs;

public:

	TAlphaParser(size_t NumTreatments, std::string args){
		_numTreatments = NumTreatments;

		//check if we use per-treatment parameters (string contains a '/')
		if(coretools::str::stringContains(args, '/')){
			_perTreatmentArgs = true;
			std::vector<std::string> tmp;
			coretools::str::fillContainerFromString<true,true>(args, tmp, '/');
			for(auto& t : tmp){
				_dist.emplace_back(t);
			}

			//check if it matches # treatments
			if(_dist.size() != NumTreatments){
				UERROR("Failed to initialize mixture distributions: number of provided mixtures (", _dist.size(), ") does not match number of treatments (", NumTreatments, ")!");
			}
		} else {
			_perTreatmentArgs = false;
			_dist.emplace_back(args);
		}

		//check if all treatments have the same number of mixtures and alpha parameters
		_numMixtures = _dist[0].numMixtures();
		if(_numMixtures < 1){
			UERROR("Failed to initialize mixture distributions: at least one distribution must be given for all treatments!");
		}
		_numAlpha = _dist[0].numAlpha();
		if(_numAlpha != 2 && _numAlpha != 4){
			UERROR("Failed to initialize mixture distributions: provided alpha values must be either 2 (for beta-binomial mixtures) or 4 (for Polya mixtures)!");
		}
		for(size_t i = 1; i < _dist.size(); ++i){
			if(_dist[i].numMixtures() != _numMixtures){
				UERROR("Failed to initialize mixture distributions: unequal number of mixtures between treatments!");
			}
			if(_dist[i].numAlpha() != _numAlpha){
				UERROR("Failed to initialize mixture distributions: unequal number of alpha values among mixtures!");
			}
		}
	}

	bool hasPerTreatmentArgs() const { return _perTreatmentArgs; }
	size_t numTreatments() const { return _numTreatments; }
	size_t numMixtures() const { return _numMixtures; }
	bool isBetaBinom() const { return _numAlpha == 2; }
	const std::vector<double>& get(size_t TreatmentIndex, size_t MixtureIndex) const {
		return _dist[TreatmentIndex][MixtureIndex];
	}
};


#endif /* CORE_TALPHAPARSERPARSER_H_ */
