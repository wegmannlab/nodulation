#ifndef TTRANSITIONMATRIXNODULATION_H_
#define TTRANSITIONMATRIXNODULATION_H_

#include "NodulationTypes.h"
#include "stattools/HMM/TTransitionMatrix.h"
#include "stattools/HMM/THMMVars.h"
#include "TPolyaMixture.h"
#include "coretools/Strings/stringFunctions.h"

#define TRANSMATMINRHO 0.0000001

//--------------------------------------------
// TTransitionMatrix_nodulation
//--------------------------------------------
class TTransitionMatrix_nodulation: public stattools::TTransitionMatrix_base<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>{
private:
	bool _isInitialized;
	using stattools::TEMPrior_base<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>::_numStates;
	using stattools::TTransitionMatrix_base<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>::_init;

    //dimensionality
    NodulationNumTreatmentType _numMixtures_D; //number of distributions in mixture
    NodulationNumTreatmentType _numTreatments_J; //number of treatments
    NodulationNumStatesType _numNonUniformStates; //number of non-uniform states: S-D

    /*NOTE: states are enumerated as follows:
            - The first D states are uniform with elements d=0, ..., D-1
            - The next S-D states are non-uniform and ordered as 0...01, 0...02, ..., 0...0(D-1), 0...10, 0...11, ..., (D-1)...(D-1)(D-2)
        The map _map_S_to_D stores these mappings
    */
   std::vector< std::vector<uint8_t> > _map_S_to_D;

    //parameters
   	NodulationPrecisionType _rho, _rhobar;
    NodulationPrecisionType _sigma, _sigmabar; //probabilities to stay
    NodulationPrecisionType _pi, _pibar; //stationary distribution

    //storage used to learn parameters via EM
    std::vector<NodulationPrecisionType> _EMSum_rho;
    std::vector<NodulationPrecisionType> _EMSum_rhobar;
    uint32_t _EMNumAdded;

    //functions
    void _calculateSigmaAndStationary();

public:
    TTransitionMatrix_nodulation();
    TTransitionMatrix_nodulation(NodulationNumTreatmentType NumMixtures_D, NodulationNumTreatmentType NumTreatments_J, double Rho, double Rhobar);
    ~TTransitionMatrix_nodulation() = default;

    using stattools::TTransitionMatrix_base<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>::numStates;

    void update(NodulationNumTreatmentType NumMixtures_D, NodulationNumTreatmentType NumTreatments_J, NodulationPrecisionType Rho, NodulationPrecisionType Rhobar);
    void update(NodulationPrecisionType Rho, NodulationPrecisionType Rhobar);
    NodulationNumTreatmentType numMixtures() const { return _numMixtures_D; };
    NodulationNumTreatmentType numTreatments() const { return _numTreatments_J; };
    NodulationNumTreatmentType mixtureOfTreatment(NodulationNumStatesType State, NodulationNumTreatmentType Treatment) const;
    bool stateIsUniform(NodulationNumStatesType State) const { return State < _numMixtures_D; }; //first D are uniform
    std::string stateString(NodulationNumStatesType State);

    //access transition / stationary probabilities
    NodulationPrecisionType rho() const { return _rho; };
    NodulationPrecisionType rhobar() const { return _rhobar; };

    NodulationPrecisionType operator()(NodulationLengthType Index, NodulationNumStatesType From, NodulationNumStatesType To) const;
    NodulationPrecisionType stationary(NodulationNumStatesType State) const;

    //EM initialization: do not initialize! We initialize outside.
    void initializeEMParameters(const stattools::TLatentVariableWithRep<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType> &,
    		                    const std::vector<NodulationLengthType> &) override {};

    //EM update
    void prepareEMParameterEstimationOneIterationTransitionProbabilities() override;
    void handleEMParameterEstimationOneIterationTransitionProbabilities(NodulationLengthType Index, const stattools::THMMPosteriorXi<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType> & xi) override;
    void finalizeEMParameterEstimationOneIterationTransitionProbabilities() override;
    void fillSumByMixture(const stattools::TDataVector<NodulationPrecisionType, NodulationNumStatesType> & Gamma, TPolyaPosteriorWeights & WeightsByMixtureAndTreatment);

    template <typename T> double sumAcrossUniform(T & vec){
    	NodulationPrecisionType res = 0.0;
    	for(NodulationNumTreatmentType d = 0; d < _numMixtures_D; ++d){
    		res += vec[d];
    	}
    	return res;
    };

    //simulation
    NodulationNumStatesType sampleFromStationary() const;
    NodulationNumStatesType sampleNextState(NodulationLengthType Index, NodulationNumStatesType State) const;

    //output
    void printStoDMap();
};


#endif
