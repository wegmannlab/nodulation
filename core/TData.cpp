
#include "TData.h"

using namespace coretools::instances;

//--------------------------------------------
// TDataCounts
//--------------------------------------------
void TDataCounts::update(const uint16_t & numA, const uint16_t & numC, const uint16_t & numG, const uint16_t & numT){
	_counts[0] = numA;
	_counts[1] = numC;
	_counts[2] = numG;
	_counts[3] = numT;

    //store sum
    _sum = std::accumulate(_counts.begin(), _counts.end(), 0);
};

void TDataCounts::addFrequencies(TFrequencies & frequencies){
	frequencies[0] += _counts[0] / (double) _sum;
	frequencies[1] += _counts[1] / (double) _sum;
	frequencies[2] += _counts[2] / (double) _sum;
	frequencies[3] += _counts[3] / (double) _sum;
};

void TDataCounts::sort(const std::vector<uint8_t> & index){
	std::array<uint16_t, 4> tmp = _counts;
	_counts[0] = tmp[index[3]];
	_counts[1] = tmp[index[2]];
	_counts[2] = tmp[index[1]];
	_counts[3] = tmp[index[0]];
};

//--------------------------------------------
// TDataFile
//--------------------------------------------
TDataFile::TDataFile(const std::string & Filename, const uint8_t & Index){
    _index = Index;

    //open file
    std::vector<std::string> cols = { (std::string) TData::chrTag, (std::string) TData::posTag, "numA", "numC", "numG", "numT"};
    _file.open(Filename, cols);

    //read first line
    advance();
};

bool TDataFile::advance(){
    if(!_file.read(_line)){
        return false;
    }

    //parse position
    _position = coretools::str::convertString<uint32_t>(_line[1]);
    return true;
};

void TDataFile::saveData(const std::string & Chr, const uint32_t & Position, TDataLocus & Locus){
    if(chr() == Chr && position() == Position){
    	Locus[_index].update(coretools::str::convertString<uint16_t>(_line[2]),
    						 coretools::str::convertString<uint16_t>(_line[3]),
							 coretools::str::convertString<uint16_t>(_line[4]),
							 coretools::str::convertString<uint16_t>(_line[5]));
    } else {
        //save empty
        Locus[_index].clear();
    }
};

//--------------------------------------------
// TData
//--------------------------------------------
TData::TData(){
	_isInitialized = false;
	_numDataSets = 0;
};

TData::TData(const uint8_t & NumTreatmenets, const uint8_t & NumReplicates, const std::vector<uint32_t> & Chromosomes){
	_isInitialized = false;
	initializeFromDimensions(NumTreatmenets, NumReplicates, Chromosomes);
};

void TData::clear(){
	if(_isInitialized){
		_loci.clear();
		_treatments.clear();
		_chromosomes.clear();
		_numDataSets = 0;
		_indexToTreatment.clear();
		_isInitialized = false;
	}
};

TDataTreatment& TData::_findTreatment(const std::string & name){
	//find by name
	for(auto& treatment : _treatments){
		if(treatment.name() == name){
			return treatment;
		}
	}
	throw "Unknown treatment with name '" + name + "'!";
};

bool TData::_treatmentExists(const std::string & name){
	for(auto& treatment : _treatments){
		if(treatment.name() == name){
			return true;
		}
	}
	return false;
};

void TData::_fillIndexMap(){
	_numDataSets = 0;
	_indexToTreatment.clear();
	uint8_t treatmentIndex = 0;
	for(auto& t : _treatments){
		for(auto& r : t){
			_indexToTreatment.push_back(treatmentIndex);
			++_numDataSets;
		}
		++treatmentIndex;
	}
};

//functions to read data
void TData::_savePosition(std::vector<TDataFile*> & DataFiles, const std::string & Chr, const uint32_t & Position){
	//create new locus
	_loci.emplace_back(_numDataSets);
	_chromosomes.incrementLastPositionOfLast();

	for(auto& d : DataFiles){
		d->saveData(Chr, Position, _loci.back());
	}
};

void TData::_jumpToNextChromosome(std::vector<TDataFile*> & DataFiles, std::string & curChr, uint32_t & curPos){
	//we assume that each file has at least one locus on each chromosome
	curChr = DataFiles[0]->chr();
	curPos = DataFiles[0]->position();
	for(auto& d : DataFiles){
		if(d->chr() == curChr && d->position() < curPos){
			curPos = d->position();
		}
	}

	//save chromosome
	_chromosomes.addChromosome(curChr, curPos);
};

void TData::sortCountsByWithinTreatmentFrequencies(){
    //sort counts by within-treatment frequency
    TFrequencies frequencies;
    std::vector<uint8_t> ranks;
    for(auto& l: _loci){
    	uint16_t index = 0;
    	for(auto& t : _treatments){
    		frequencies.clear();

    		for(uint8_t r=0; r < t.numReplicates(); ++r){
    			l[index+r].addFrequencies(frequencies);
    		}

    		coretools::rankSort(frequencies, ranks);
    		for(uint8_t r=0; r < t.numReplicates(); ++r){
    			l[index].sort(ranks);
    			++index;
    		}
    	}
    }
};

void TData::initializeFromFiles(){
	logfile().startIndent("Reading data:");
	//make sure data is empty
	clear();

    //open data files
    //---------------
    //read data files from file
    std::string filename = parameters().getParameter<std::string>("data");
    logfile().listFlush("Reading data files from '" + filename + "' (argument 'data') ... ");

    coretools::TInputFile infile(filename, 2);
    
    std::vector<std::string> line;
    while(infile.read(line)){
        //make sure treatment exists
        if(!_treatmentExists(line[1])){
            _treatments.emplace_back(line[1]);
        }

        //find corresponding treatment
        TDataTreatment& treatment = _findTreatment(line[1]);

        //add replicate to treatment
        if(treatment.replicateExists(line[0])){
            throw "File '" + line[0] + "' is listed multiple times in '" + filename + "'!";
        }
        treatment.addReplicate(line[0]);
    }
    logfile().done();

    //check if there are some treatments
    if(_treatments.size() < 2){
    	throw "Need data from at least two treatmenets!";
    }

    //open files
    logfile().startIndent("Will read data for the following treatments:");

    std::vector<TDataFile*> dataFiles;
    uint16_t index = 0;
    for(auto& t : _treatments){
        logfile().startIndent(t.name() + ":");
        for(auto& s : t){
        	logfile().list(s);
        	dataFiles.push_back(new TDataFile(s, index));
        	++index;
        }
        logfile().endIndent();
    }
    logfile().endIndent();
    _fillIndexMap();
    
    //now parse through all data files and add data
    //---------------------------------------------
    uint32_t limitSites;
    if(parameters().parameterExists("limitSites")){
    	limitSites = parameters().getParameter<uint32_t>("limitSites");
    	logfile().list("Will limit data to the first ", limitSites, " sites. (argument 'limitSites')");
    } else {
    	limitSites = std::numeric_limits<uint32_t>::max();
    	logfile().list("Will read all sites in files. (use 'limitSites' to limit)");
    }

    logfile().listFlushTime("Reading data files ...");

    //get first chr and position.
    std::string curChr; uint32_t curPos;
    _jumpToNextChromosome(dataFiles, curChr, curPos);
    
    //loop over files
    bool isGood = true;
    uint32_t numSitesRead = 0;
    while(isGood && numSitesRead < limitSites){
    	isGood = false;
        size_t numFilesOnCurChr = 0;
        for(auto& d : dataFiles){
            while(d->isOpen() && d->chr() == curChr && d->position() < curPos){
                d->advance();
            }
            if(d->isOpen()){
            	isGood = true;
            	if(d->chr() == curChr){
            		++numFilesOnCurChr;
            	}
            }
        }
        
        //check if we advance to next chromosome
        if(numFilesOnCurChr > 0){
        	_savePosition(dataFiles, curChr, curPos);

        	//advance position
        	++curPos;
        	++numSitesRead;
        } else if(isGood){
            //advance to next chromosome
        	_jumpToNextChromosome(dataFiles, curChr, curPos);
        }
    }

    //clean up
    for(auto& d : dataFiles){
    	delete d;
    }

    logfile().doneTime();
    logfile().conclude("Read data at ", _chromosomes.totalLength(), " loci on ", _chromosomes.size(), " chromosomes.");
    logfile().endIndent();
};

void TData::initializeFromDimensions(const uint8_t & NumTreatmenets, const uint8_t & NumReplicates, const std::vector<uint32_t> & Chromosomes){
	//make sure data is empty
	clear();

	//create treaments and replicates
	for(uint8_t t=0; t<NumTreatmenets; ++t){
		_treatments.emplace_back((std::string) "Treatment" + static_cast<char>('A' + t));
		for(uint8_t r=0; r < NumReplicates; ++r){
			_treatments.back().addReplicate("Replicate" + coretools::str::toString(r+1));
		}
	}

	//fill index map
	_fillIndexMap();

	//create chromosomes and loci
	uint32_t numLoci = 0;
	for(size_t c = 0; c < Chromosomes.size(); ++c){
		_chromosomes.addChromosome("chr" + coretools::str::toString(c+1), 0, Chromosomes[c]);
		numLoci += Chromosomes[c];
	}

	TDataLocus locus(_numDataSets);
	_loci.insert(_loci.end(), numLoci, locus);
};

void TData::fillHMMJunkEnds(std::vector<uint32_t> & JunkEnds) const{
	JunkEnds.clear();
	uint32_t previousEnd = 0;
	for(std::vector<TChromosome>::const_iterator it = _chromosomes.cbegin(); it != _chromosomes.cend(); ++it){
		previousEnd += it->length();
		JunkEnds.push_back(previousEnd);
	}
};

std::vector<size_t> TData::getDataIndecesAllTreatments() const {
	std::vector<size_t> indeces;
	indeces.reserve((size_t) _numDataSets);
	for(NodulationNumTreatmentType i = 0; i < _numDataSets; ++i){
		indeces.push_back((size_t) i);
	}
	return indeces;
}

std::vector<size_t> TData::getDataIndeces(NodulationNumTreatmentType Treatment) const{
	std::vector<size_t> indeces;
	for(NodulationNumTreatmentType i = 0; i < _numDataSets; ++i){
		if(treatmentIndex(i) == Treatment){
			indeces.push_back((size_t) i);
		}
	}
	return indeces;
}

void TData::write(const std::string & BaseName){
	logfile().startIndent("Writing count data to files:");

	//open files
	logfile().startIndent("Opening files:");

	//data file
	std::string filename = BaseName + "data.txt";
	logfile().list("Writing data set to file '" + filename + ".");
	coretools::TOutputFile data(filename);

	//count files
	std::vector<coretools::TOutputFile*> countFiles;
	std::vector<std::string_view> header = {chrTag, posTag, "numA", "numC", "numG", "numT"};
	for(auto& t : _treatments){
		logfile().startIndent("Writing replicates of treatment '" + t.name() + "' to files:");
		for(uint8_t r = 0; r < t.numReplicates(); ++r){
			filename = BaseName + "counts_" + t.name() + "_" + coretools::str::toString(r + 1) + ".txt.gz";
			logfile().list(filename);
			countFiles.push_back(new coretools::TOutputFile(filename, header));
			data.writeln(filename, t.name());
		}
		logfile().endIndent();
	}
	logfile().endIndent();
	data.close();

	//write data
	logfile().startIndent("Writing data:");
	std::vector<TDataLocus>::iterator l = _loci.begin();
	for(auto& c : _chromosomes){
		logfile().listFlush("Writing chromosome '" + c.name() + "' ...");
		for(uint32_t pos = c.firstPosition(); pos < c.lastPosition(); ++pos, ++l){
			if(l == _loci.end()){
				throw std::runtime_error("void TData::write(const std::string & BaseName, TLog* Logfile): reached end of loci!");
			}
			TDataLocus& locus = *l;
			for(uint8_t r = 0; r < _numDataSets; ++r){
				countFiles[r]->writeln(c.name(), pos, locus[r][0], locus[r][1], locus[r][2], locus[r][3]);
			}
		}
		logfile().done();
	}
	logfile().endIndent();

	//close files
	for(auto& f : countFiles){
		delete f;
	}
	logfile().endIndent();
};

void TData::writePosition(const uint32_t & locus, coretools::TOutputFile & out){
	//map index to chromosome
	auto c = _chromosomes.begin();
	uint32_t tot = 0;

	while(tot + c->length() < locus){
		tot += c->length();
		++c;
		if(c == _chromosomes.end()){
			throw std::runtime_error("void TData::writePosition(const uint32_t & index, TOutputFile & out): index is past last chromosome!");
		}
	}

	//write chr name and position
	out << c->name() << c->firstPosition() - tot + locus + 1;
};
