

#ifndef TSIMULATOR_H_
#define TSIMULATOR_H_

#include "TData.h"
#include "TPolyaMixture.h"
#include "TTransitionMatrixNodulation.h"
#include "coretools/Main/TTask.h"
#include "coretools/Main/globalConstants.h"

using namespace coretools;
using namespace stattools;

//-------------------------------------
// TSimulator
//-------------------------------------
class TSimulator{
private:
	TData _data;
	std::unique_ptr<TMixture_base> _mixture;
	TTransitionMatrix_nodulation _transMat;
	uint16_t _N;
	std::string _outName;

	void _simulateLocus(const uint16_t & State, uint32_t & Locus, const TChromosome & Chromosome, TOutputFile & TrueStateFile);

public:
	TSimulator();
	~TSimulator() = default;

	void run();
};

//------------------------------------------------
// Task_simulate
//------------------------------------------------
class TTask_simulate:public TTask{
public:
	//constructor that sets explanation
	TTask_simulate(){ _explanation = "simulate data"; };

	//run function
	void run(){
		TSimulator simulator;
		simulator.run();
	};
};


#endif // TSIMULATOR_H_
