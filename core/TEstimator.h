/*
 * TEstimator.h
 *
 *  Created on: Dec 8, 2020
 *      Author: phaentu
 */

#ifndef CORE_TESTIMATOR_H_
#define CORE_TESTIMATOR_H_

#include "TData.h"
#include "TTransitionMatrixNodulation.h"
#include "TPolyaMixture.h"
#include "stattools/HMM/THMM.h"
#include "coretools/Main/globalConstants.h"
#include "coretools/Main/TTask.h"
#include "NodulationTypes.h"
#include <limits>

//-------------------------------------
// TLatenVariable_polya
//-------------------------------------
class TLatenVariable_polya : public stattools::TLatentVariable<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>{
private:
	TData _data;
	std::unique_ptr<TMixture_base> _mixture;
	bool _updateMixtures;
	TTransitionMatrix_nodulation _transitionMatrix;

	// EM weights
	TPolyaPosteriorWeights _polyaPosteriorWeights;

	//Minka optimization parameters
	bool _doMinka;
	double _minDiffPolyaOptimization;
	uint16_t _maxNumIterationsPolyaOptimization;

	// NelderMead optimization parameters
	double _initialDisplacement_NelderMead;
	double _fractionalConvergenceTolerance_NelderMead;
	size_t _maxNumFuncEvaluations_NelderMead;
	double _factorDynamicTolerance_NelderMead;
	size_t _numFunctionCallsUntilAdjustingTolerance_NelderMead;
	bool _firstEMIteration;

	//file to write output
	coretools::TOutputFile out;

	void _readCommandLineArgs_Minka();
	void _readCommandLineArgs_NelderMead();

public:
	TLatenVariable_polya();
	~TLatenVariable_polya() = default;

	// Emission probabilities
	void calculateEmissionProbabilities(NodulationLengthType Index, stattools::TDataVector<NodulationPrecisionType, NodulationNumStatesType> & Emission) const;

	// EM functions
	void prepareEMParameterEstimationOneIteration() override;
	void handleEMParameterEstimationOneIteration(NodulationLengthType index, const stattools::TDataVector<NodulationPrecisionType, NodulationNumStatesType> & Weights) override;
	void finalizeEMParameterEstimationOneIteration() override;
	void reportEMParameters() override;
	void _reportCurrentParameterEstimates();

	// writing of posterior file
	void prepareStatePosteriorEstimationAndWriting(NodulationNumStatesType NumStates, std::vector<std::string> & header) override;
	void handleStatePosteriorEstimationAndWriting(NodulationLengthType Index,
			                                      const stattools::TDataVector<NodulationPrecisionType, NodulationNumStatesType> & StatePosteriorProbabilities,
												  coretools::TOutputFile & out) override;

	//getters of objects
	const TData& data() const { return _data; };
	const TTransitionMatrix_nodulation& transitionMatrix() const { return _transitionMatrix; };
	TTransitionMatrix_nodulation& transitionMatrix() { return _transitionMatrix; };
};

//-------------------------------------
// TEstimator
//-------------------------------------
class TEstimator{
private:
	TLatenVariable_polya* _latentVariable;
	stattools::THMM<NodulationPrecisionType, NodulationNumStatesType, NodulationLengthType>* _hmm;
	std::string _outName;

	void _reportHierarchicalParameterestimates();

public:
	TEstimator();
	~TEstimator();

	void runBaumWelch();
	void estimateStatePosteriors();
	void calculateLL();
};


//------------------------------------------------
// Task_estimate
//------------------------------------------------
class TTask_estimate:public coretools::TTask{
public:
	//constructor that sets explanation
	TTask_estimate(){ _explanation = "Estimate parameters using a Baum-Welch algorithm"; };

	//run function
	void run(){
		TEstimator estimator;
		estimator.runBaumWelch();
	};
};

//------------------------------------------------
// Task_statePosterior
//------------------------------------------------
class Task_statePosteriors:public coretools::TTask{
public:
	//constructor that sets explanation
	Task_statePosteriors(){ _explanation = "Estimate state posteriors"; };

	//run function
	void run(){
		TEstimator estimator;
		estimator.estimateStatePosteriors();
	};
};

//------------------------------------------------
// Task_calculateLL
//------------------------------------------------
class Task_calculateLL:public coretools::TTask{
public:
	//constructor that sets explanation
	Task_calculateLL(){ _explanation = "Caluclate log-likelihood of parameters"; };

	//run function
	void run(){
		TEstimator estimator;
		estimator.estimateStatePosteriors();
	};
};

#endif /* CORE_TESTIMATOR_H_ */
