
#include <TAlphaParserParser.h>
#include "TSimulator.h"

using namespace coretools::instances;


//-------------------------------------
// TSimulator
//-------------------------------------

TSimulator::TSimulator(){
	logfile().startIndent("Reading simulation parameters:");

	//reading data size
	uint8_t numTreatments = parameters().getParameterWithDefault<uint8_t>("treatments", 2);
	logfile().list("Will simulate ", numTreatments, " treatments. (argument 'alpha')");

	uint8_t numReplicates = parameters().getParameterWithDefault<uint8_t>("replicates", 3);
	logfile().list("Will simulate ", numReplicates, " replicates per treatment. (argument 'replicates')");

	_N = parameters().getParameter<uint16_t>("N", "The number of samples taken per replicate.");
	logfile().list("Will simulate ", _N, " samples per replicate. (argument 'N')");

	std::vector<uint32_t> chrLength;
	parameters().fillParameterIntoContainer("chr", chrLength, ',');
	logfile().list("Will simulate ",chrLength.size(), " chromosomes of lengths: " + coretools::str::concatenateString(chrLength, ", ") + ". (argument 'chr')");

	//creating TData
	_data.initializeFromDimensions(numTreatments, numReplicates, chrLength);

	//create mixture distributions
	TAlphaParser alphaParser(numTreatments, parameters().getParameter("alpha"));
	createMixture(_mixture, alphaParser);
	_mixture->reportCurrentParameters(_data);

	//create transition matrix
	double rho = parameters().getParameterWithDefault("rho", 0.1);
	double rhobar = parameters().getParameterWithDefault("rhobar", 0.1);
	logfile().list("Will use a transition matrix with rho = ", rho, " and rhobar = ", rhobar, ".");
	_transMat.update(_mixture->numMixtures(), numTreatments, rho, rhobar);

	//outname
	_outName = parameters().getParameterWithDefault("out", __GLOBAL_APPLICATION_NAME__ + "_");
	logfile().list("Will write output files with prefix '" + _outName + "'. (argument 'out')");

	logfile().endIndent();
};

void TSimulator::_simulateLocus(const uint16_t & State, uint32_t & Locus, const TChromosome & Chromosome, TOutputFile & TrueStateFile){
	TrueStateFile << Chromosome.name() << Chromosome.firstPosition() + Locus << State;

	//mixtures per treatment
	for(NodulationNumTreatmentType t = 0; t < _data.numTreatments(); ++t){
		TrueStateFile << (int) _transMat.mixtureOfTreatment(State, t);
	}
	TrueStateFile.endln();

	for(NodulationNumTreatmentType d = 0; d < _data.numDataSets(); ++d){
		_mixture->simulateCounts(_N, _transMat.mixtureOfTreatment(State, _data.treatmentIndex(d)), _data.treatmentIndex(d), _data[Locus][d]);
	}

	++Locus;
};

void TSimulator::run(){
	logfile().startIndent("Running simulations:");

	//open file to write true state
	std::string filename = _outName + "trueStates.txt.gz";
	logfile().list("Writing true states to '" + filename + "'.");
	std::vector<std::string> header = {"chromosome", "position", "state"};
	for(NodulationNumTreatmentType t = 0; t < _data.numTreatments(); ++t){
		header.push_back("mixture" + _data.getTreatmentName(t));
	}
	TOutputFile trueStateFile(filename, header);

	//loop over chromosomes
	NodulationLengthType locus = 0;
	for(NodulationLengthType c = 0; c < _data.chromosomes().size(); ++c){
		const TChromosome& chr = _data.chromosomes()[c];

		logfile().listFlush("Simulating ", chr.length(), " loci on chromosome ", chr.name(), " ...");

		//sample initial state
		NodulationNumStatesType state = _transMat.sampleFromStationary();

		//sample data at first locus
		_simulateLocus(state, locus, chr, trueStateFile);

		//loop over all other loci of this chromosome
		NodulationLengthType length = chr.length();
		for(NodulationLengthType l=1; l<length; ++l){
			//transition to new state
			state = _transMat.sampleNextState(l, state);

			//simulate data
			_simulateLocus(state, locus, chr, trueStateFile);
		}

		//report
		logfile().done();
	}
	trueStateFile.close();
	logfile().endIndent();

	//write data
	_data.write(_outName);
	logfile().endIndent();
};




